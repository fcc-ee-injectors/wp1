%% Load RF-Track
RF_Track;

%% Bunch parameters
mass = RF_Track.electronmass; % MeV/c^2
charge = -1; % single-particle charge, in units of e
population = 3.4 * RF_Track.nC; % number of real particles per bunch
P0 = 1540; % MeV/c, initial reference momentum
P1 = 6000; % MeV/c, final reference momentum
emitt_initial = 10; % mm.mrad, initial normalized emittance
                    % multi-bunch
S_bunch = 17.5 * RF_Track.ns / 1e3; % m, intra-bunch separation
N_bunches = 50;

for dQ_Q = [ 0  0.01 ] % rms bunch-to-bunch charge variation

    %% Accelerating structure
    Lstr = 3; % m
    Grad = 25; % MV/m
    Phid = 0; % deg, rf phase
    freq = 3; % Ghz

    %% FODO cell paramters
    Nstr = 8; % number of structures per FODO cell
    Lcell = Nstr*Lstr; % m, two structures
    Lquad = 0; % m, thin lens
    Ldrift = Lcell/2 - Lquad; % m
    mu = 90; % deg, phase advance
    k1L = sind(mu/2) / (Lcell/4); % 1/m

    %% Long-range wakefield
    Kick_t = 0; % V/pC/m/mm
    Kick_l = 100; % V/pC/m

    Wt = [ 0 Kick_t 0 ]; % V/pC/m/mm
    Wl = [ 0 Kick_l 0 ]; % V/pC/m

    WF = Wakefield_1d_LINT(Wt, Wl, S_bunch, 1000);

    %% Accelerating field
    % RF = TW_Structure(-Grad*1e6, 0, freq*1e9, ph_adv, -n_cells);
    % RF.set_phid(Phid);
    RF = Drift(Nstr*Lstr/2);
    RF.set_static_Efield(0, 0, -Grad*1e6);
    RF.set_odeint_algorithm('rk2');
    RF.set_odeint_epsabs(1e-6);
    RF.set_cfx_nsteps(100);
    RF.add_collective_effect(WF);

    % Setup the lattice
    L = Lattice();
    Pref = P0;
    while Pref <= P1
        Pref
        B_rho = Pref / charge; % MV/c, reference rigidity
        L.append(Quadrupole(Lquad/2, k1L * B_rho / 2));
        L.append(RF);
        Pref += Grad * RF.get_length(); % MeV/c
        B_rho = Pref / charge; % MV/c, reference rigidity
        L.append(Quadrupole(Lquad, -k1L * B_rho));
        L.append(RF);
        Pref += Grad * RF.get_length(); % MeV/c
        B_rho = Pref / charge; % MV/c, reference rigidity
        L.append(Quadrupole(Lquad/2, k1L * B_rho / 2));
    end

    %% Create the beam
    N = (1:N_bunches)';
    O = zeros(N_bunches,1);
    I = ones(N_bunches,1);
    R = randn(N_bunches,1);
    R(1:5) = 1.1;
    M0 = [ O O O O (N-1)*S_bunch*1e3 I*P0 I*RF_Track.electronmass -I population.*(1 .+ dQ_Q*R) ];
    B0 = Bunch6d(M0);

    %% Perform tracking
    B1 = L.track(B0);

    %% Retrieve the Twiss plot and the phase space
    T = L.get_transport_table('%S %mean_x %mean_y %mean_Pz');
    M1 = B1.get_phase_space();

    stdP = std(M1(:,6))
    stdP_P = std(M1(:,6)) / mean(M1(:,6))
    rangeP = range(M1(:,6))

    %% Make plots
    figure(1)
    if dQ_Q == 0
        clf
        hold on
    end
    axis([ 0 N_bunches 90 105 ] )
    xlabel('bunch #');
    ylabel('P_{bunch} / 6 GeV/c [%]');
    if dQ_Q == 0
        plot(M1(:,6) / max(M1(:,6)) * 100, 'r-*', 'linewidth', 2);
        lgnd = legend({ 'W_{long} = 100 V/pC/m  ' });
    else
        plot(M1(:,6) / max(M1(:,6)) * 100, 'b-*', 'linewidth', 2);
        lgnd = legend({ 'W_{long} = 100 V/pC/m  ', 'dQ/Q = 1% rms' });
    end
    legend boxoff
    set(lgnd, 'fontsize', 14);

    if dQ_Q == 0
        print -dpng plot_beam_loading_example.png
    else
        lgnd = legend({ 'W_{long} = 100 V/pC/m  ', 'dQ/Q = 1% rms' });
        print -dpng plot_beam_loading_example_dQ_Q.png
    end
end