%% Load RF-Track
RF_Track;

%% Bunch parameters
mass = RF_Track.electronmass; % MeV/c^2
charge = -1; % single-particle charge, in units of e
P0 =  200; % MeV/c, initial reference momentum
P1 = 1540; % MeV/c, final reference momentum
emitt_initial = 10; % mm.mrad, initial normalized emittance                  
S_bunch = 17.5 * RF_Track.ns / 1e3; % m, intra-bunch separation
N_bunches = 50;

dQ_Q = 0.01; % rms bunch-to-bunch charge variation

%% Accelerating structure
Lstr = 1.25; % m
Grad = 18; % MV/m
frequency = 2.9985; % GHz

%% FODO cell paramters
Nstr = 8; % number of structures per FODO cell
Lcell = Nstr*Lstr; % m, two structures
Lquad = 0; % m, thin lens
Ldrift = Lcell/2 - Lquad; % m
mu = 90; % deg, phase advance
k1L = sind(mu/2) / (Lcell/4); % 1/m

%% main loop
DATA = zeros(19, 17);

iQ = 0;
population_range = linspace(2, 6, 17) * RF_Track.nC;
for population = population_range; % number of real particles per bunch
    iQ++;
    iWt = 0;
    Kick_l_range = linspace(0, 100, 19); % V/pC/m
    for Kick_l = Kick_l_range % V/pC/m

        iWt++;

        %% Long-range wakefield
        Wt = [ 0 0 0 ]; % V/pC/m/mm
        Wl = [ 0 Kick_l 0 ]; % V/pC/m
        
        WF = Wakefield_1d_LINT(Wt, Wl, S_bunch, 1000);
        
        %% Accelerating field
        % RF = TW_Structure(-Grad*1e6, 0, freq*1e9, ph_adv, -n_cells);
        RF = Drift(Nstr*Lstr/2);
        RF.set_static_Efield(0, 0, -Grad*1e6);
        RF.set_odeint_algorithm('rk2');
        RF.set_odeint_epsabs(1e-6);
        RF.set_cfx_nsteps(10);
        RF.add_collective_effect(WF);
        
        % Setup the lattice
        L = Lattice();
        Pref = P0;
        while Pref <= P1
            Pref
            B_rho = Pref / charge; % MV/c, reference rigidity
            L.append(Quadrupole(Lquad/2, k1L * B_rho / 2));
            L.append(RF);
            Pref += Grad * RF.get_length(); % MeV/c
            B_rho = Pref / charge; % MV/c, reference rigidity
            L.append(Quadrupole(Lquad, -k1L * B_rho));
            L.append(RF);
            Pref += Grad * RF.get_length(); % MeV/c
            B_rho = Pref / charge; % MV/c, reference rigidity
            L.append(Quadrupole(Lquad/2, k1L * B_rho / 2));
        end

        %% Create the beam
        randn('seed', 12345);
        for seed = 1:10
            N = (1:N_bunches)';
            O = zeros(N_bunches,1);
            I = ones(N_bunches,1);
            R = randn(N_bunches,1);
            M0 = [ O O O O (N-1)*S_bunch*1e3 I*P0 I*RF_Track.electronmass -I population.*(1 .+ dQ_Q*R) ];
            B0 = Bunch6d(M0);
            
            %% Perform tracking
            B1 = L.track(B0);
            
            %% Retrieve the Twiss plot and the phase space
            T = L.get_transport_table('%S %mean_x %mean_y %mean_Pz');
            M1 = B1.get_phase_space();
            
            stdP = std(M1(:,6));
            stdP_P = std(M1(:,6)) / mean(M1(:,6));
            rangeP = range(M1(:,6));
            
            DATA(iWt, iQ) = max(DATA(iWt, iQ), rangeP);
        end
    end
end

DATA = DATA ./ P1 * 100; % percent

save -text beam_loading_dQ_Q_1p.dat DATA

figure(1)
clf
%% Make plots
colormap('cool');
[C,H] = contourf(population_range / RF_Track.nC, Kick_l_range, DATA, ...
                 [ 0 0.1 0.2:.2:1 1.2 1.5:.5:7 ]);
xlabel('Bunch charge [nC]');
ylabel('W_l [V/pC/m]');
clabel(C, H, 'color', 'white');
h = colorbar;
set(h, 'fontsize', 10);
set(get(h, 'label'), 'string', ['max(range(\Delta{P}/P)) for 100 seeds [%]'], 'fontsize', 10);
set(gca, 'fontsize', 10);

print -dpng plot_multi_bunch_beam_loading_scan_dQ_Q_1p.png

system('for i in *png;  do anytopnm $i | pnmcrop -sides | pnmtopng > tmp.png && mv tmp.png $i ; done');
