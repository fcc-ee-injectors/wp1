addpath('scripts/');

%% Load RF-Track
RF_Track;

for Structure_type = 1:4

    figure(1);
    clf ; hold on

    figure(2);
    clf ; hold on

    figure(3);
    clf ; hold on
    
    title_Er = 'Offset = 1 mm';
    title_Ez = 'Offset = 1 mm';

    for Phid = [ 0 8 ]

        %% Bunch parameters
        mass = RF_Track.electronmass; % MeV/c^2
        charge = -1; % single-particle charge, in units of e
        population = 5.5 * RF_Track.nC; % number of real particles per bunch
        P0 = 1540; % MeV/c, initial reference momentum
        P1 = 6000; % MeV/c, final reference momentum
        emitt_initial = 5; % mm.mrad, initial normalized emittance
        sigma_z = 1; % mm
        Pspread = 0.1; % percent, momentum spread

        %% Accelerating structure
        Grad = 25; % MV/m
        % Phid = 0; % deg, rf phase
        freq = 2.8; % Ghz

        % short-range wakes and cell length
        Lstr = 3; % m, structure length
        a_over_lambda = 0.15; %
        lambda = 299.792458 / freq; % mm, RF wavelength
        ph_adv = 2*pi/3; % radian, phase advance per cell
        l = lambda * ph_adv / (2*pi); % mm, cell length
        a = a_over_lambda * lambda; % mm, iris aperture
        g = l - 3; % mm, gap length
        SRWF = ShortRangeWakefield(a/1e3, g/1e3, l/1e3);

        %% Setup the lattice
        switch Structure_type
          case 1 % constant field structure
            RF = Drift(Lstr);
            RF.set_static_Efield(0, 0, -Grad*1e6*cosd(Phid)); 
            RF.enable_end_fields();
            RF.set_odeint_algorithm('leapfrog');
            RF.set_odeint_epsabs(1e-3);
          case 2 % analytic tw structure
            Lstr = 3; % m
            n_cells = 3*floor(ceil(Lstr*1e3 / l)/3); % number of cells, negative sign indicates a start from the beginning of the cell
            RF = TW_Structure(-Grad*1e6, 0, freq*1e9, ph_adv, n_cells);
            RF.set_phid(Phid);
            RF.set_odeint_algorithm('leapfrog');
            RF.set_odeint_epsabs(1e-3);
          case 3 % field map extracted from the analytic solution + fake end fields
            Lstr = 3; % m
            n_cells = 3*floor(ceil(Lstr*1e3 / l)/3); % number of cells, negative sign indicates a start from the beginning of the cell
            TW = TW_Structure(-Grad*1e6, 0, freq*1e9, ph_adv, n_cells);
            TW.set_t0(0.0);
            Za = linspace(0, TW.get_length()*1e3, 8400); % mm
            O = zeros(size(Za)); % mm
            TW.set_phid(0.0);   [E,~] = TW.get_field(O, O, Za, O); Ez_real = E(:,3);
            TW.set_phid(-90.0); [E,~] = TW.get_field(O, O, Za, O); Ez_imag = E(:,3);
            Ez_ = complex(Ez_real, Ez_imag); % V/m
            if 1 % tries to make end fields going to zero at the ends
                Ez_(1:300) .*= linspace(0, 1, 300)';
                Ez_(end-299:end) .*= linspace(1, 0, 300)';
                Ez_(1:300) = real(Ez_(1:300));
                Ez_(end-299:end) = real(Ez_(end-299:end));
            end
            RF = RF_FieldMap_1d (Ez_, (Za(2) - Za(1))/1e3, -1, freq*1e9, +1);
            RF.set_odeint_algorithm('leapfrog');
            RF.set_odeint_epsabs(1e-3);
            RF.set_phid(Phid);
            RF.unset_t0();
          case 4 % field map of TW
            RF = load_tws(Grad*1.5, Phid);
            RF.set_odeint_algorithm('leapfrog');
            RF.set_odeint_epsabs(1e-3);
            RF.set_phid(Phid);
          otherwise
            error('error in script');
        end

        RF.set_nsteps(100);
        RF.set_cfx_nsteps(10);
        RF.add_collective_effect(SRWF);
        
        %% Gets the momentum gain per struture
        P = Bunch6d(mass, population, charge, [ 0 0 0 0 0 P0 ]);
        L = Lattice();
        L.append_ref(RF);
        L.autophase(P);
        
        %% Plot field
        Za = linspace(0, RF.get_length()*1e3, 1000); % mm
        O = zeros(size(Za)); % mm
        [E,B] = RF.get_field(O, O, Za, O);
        Ez = E(:,3)/1e6;
        
        figure(1)
        plot(Za/1e3, Ez, '-', 'linewidth', 2, 'displayname', num2str(Phid));
        grid
        ylim([ -40 40 ]);
        xlabel('L [m]');
        ylabel('E_z [MV/m]');
        set(gca, 'linewidth', 2, 'fontsize', 14);

        %% Plot field seen by particle
        Za = linspace(0, RF.get_length()*1e3, 1000); % mm
        O = zeros(size(Za)); % mm
        I = ones (size(Za)); % mm
        
        [E,B] = RF.get_field(I, O, Za, Za);
        Ex = E(:,1)/1e6;
        Ez = E(:,3)/1e6;

        figure(2)
        title_Ez = [ title_Ez sprintf('; <E_z> = %.2f MV/m (%d deg)', mean(Ez), Phid) ];
        title(title_Ez);
        plot(Za/1e3, Ez, 'linewidth', 2, 'displayname', num2str(Phid));
        xlabel('L [m]');
        ylabel('E_z [MV/m]');
        ylim([ -40 0 ]);
        legend
        grid

        figure(3)
        title_Er = [ title_Er sprintf('; <E_r> = %.2f MV/m (%d deg)', mean(Ex), Phid) ];
        title(title_Er);
        plot(Za/1e3, Ex, 'linewidth', 2, 'displayname', num2str(Phid));
        xlabel('L [m]');
        ylabel('E_r [MV/m]');
        legend
        grid

    end

    figure(1); print('-dpng',  sprintf('plot_field_%d.png', Structure_type));
    figure(2); print('-dpng',  sprintf('plot_field_particle_Ez_%d.png', Structure_type));
    figure(3); print('-dpng',  sprintf('plot_field_particle_Er_%d.png', Structure_type));
    
end
