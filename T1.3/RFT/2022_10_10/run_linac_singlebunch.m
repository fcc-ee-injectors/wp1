%% Load RF-Track
RF_Track;

%% Bunch parameters
mass = RF_Track.electronmass; % MeV/c^2
charge = -1; % single-particle charge, in units of e
population = 5.5 * RF_Track.nC; % number of real particles per bunch
P0 = 1540; % MeV/c, initial reference momentum
P1 = 6000; % MeV/c, final reference momentum
emitt_initial = 5; % mm.mrad, initial normalized emittance
sigma_z = 1; % mm
Pspread = 0.1; % percent, momentum spread

%% Accelerating structure
Grad = 25; % MV/m
Phid = 0; % deg, rf phase
freq = 2.8; % Ghz

% short-range wakes and cell length
a_over_lambda = 0.15; %
lambda = 299.792458 / freq; % mm, RF wavelength
ph_adv = 2*pi/3; % radian, phase advance per cell
l = lambda * ph_adv / (2*pi); % mm, cell length
a = a_over_lambda * lambda; % mm, iris aperture
g = l - 3; % mm, gap length
SRWF = ShortRangeWakefield(a/1e3, g/1e3, l/1e3);

% field
n_cells = 84; % number of cells, negative sign indicates a start from the beginning of the cell
RF = TW_Structure(-Grad*1e6, 0, freq*1e9, ph_adv, -n_cells);
RF.set_phid(Phid);
RF.set_odeint_algorithm('rk2');
RF.set_odeint_epsabs(1e-6);
RF.set_tt_nsteps(50);
RF.set_cfx_nsteps(10);
RF.add_collective_effect(SRWF);
Lstr = RF.get_length();

%RF = Drift(Lstr);
%RF.set_static_Efield(0, 0, -Grad*1e6);

%% FODO cell paramters
Nstr = 2; % number of structures per FODO cell
Lquad = 0.25; % m, thin lens
Ldrift = 0.25; % m
Lcell = Nstr*Lstr + 2*Lquad + 4*Ldrift; % m, two structures
mud = 90; % deg, phase advance
k1 = sind(mud/2) / (Lcell/4) / Lquad; % 1/m

%% Define Twiss parameters
Twiss = Bunch6d_twiss();
Twiss.emitt_x = emitt_initial; % mm.mrad, normalized emittances
Twiss.emitt_y = emitt_initial; % mm.mrad
Twiss.beta_x = Lcell * (1 + sind(mud/2)) / sind(mud); % m
Twiss.beta_y = Lcell * (1 - sind(mud/2)) / sind(mud); % m
Twiss.sigma_t = sigma_z; % mm/c
Twiss.sigma_d = Pspread * 10; % permill

%% Create the bunch
B0 = Bunch6d(mass, population, charge, P0, Twiss, 10000);

L = Lattice();
L.append(RF);
Pgain = L.autophase(B0) - P0

%% Setup the lattice
Pref = P0;
B_rho = Pref / charge; % MV/c, reference rigidity
Qf = Quadrupole(Lquad/2, B_rho, k1);
L = Lattice();
L.append(Qf); % half quadrupole
while Pref <= P1
    %%
    L.append(Drift(Ldrift));
    L.append(RF);
    Pref += Pgain; % MeV/c
    B_rho = Pref / charge; % MV/c, reference rigidity
    L.append(Drift(Ldrift));
    Qd = Quadrupole(Lquad, B_rho, -k1);
    L.append(Qd);
    %%
    L.append(Drift(Ldrift));
    L.append(RF);
    Pref += Pgain; % MeV/c
    B_rho = Pref / charge; % MV/c, reference rigidity
    L.append(Drift(Ldrift));
    Qf = Quadrupole(Lquad, B_rho, k1);
    L.append(Qf);
end

%% Perform tracking
tic
Pmax = L.autophase(B0)
B1 = L.track(B0);
toc


%% Retrieve the Twiss plot and the phase space
T = L.get_transport_table('%S %beta_x %beta_y %emitt_x %emitt_y %mean_Pz');
M = B1.get_phase_space('%x %xp %y %yp');

%% Make plots
figure(1);
clf
hold on
plot(T(:,1), T(:,2), 'b-*', 'linewidth', 2);
plot(T(:,1), T(:,3), 'r-*', 'linewidth', 2);
lgnd = legend({ '\beta_x ', '\beta_y ' });
xlabel('S [m]');
ylabel('\beta [m]');
legend boxoff
legend boxon
set(lgnd, 'fontsize', 14);
set(gca, 'linewidth', 2, 'fontsize', 14);
box
print -dpng plot_beta.png

figure(2)
clf
hold on
plot(T(:,1), T(:,4), 'b-*', 'linewidth', 2);
plot(T(:,1), T(:,5), 'r-*', 'linewidth', 2);
lgnd = legend({ '\epsilon_x ', '\epsilon_y ' });
xlabel('S [m]');
ylabel('\epsilon [mm.mrad]');
legend boxoff
legend boxon
set(lgnd, 'fontsize', 14);
set(gca, 'linewidth', 2, 'fontsize', 14);
box
print -dpng plot_emitt.png

figure(3)
clf
hold on
plot(T(:,1), T(:,6), 'r-*', 'linewidth', 2);
xlabel('S [m]');
ylabel('Pz [MeV/c]');
legend boxoff
legend boxon
set(lgnd, 'fontsize', 14);
set(gca, 'linewidth', 2, 'fontsize', 14);
box
print -dpng plot_Pz.png

figure(4)
clf
hold on
scatter(M(:,1), M(:,2), '*');
xlabel('x [mm]');
ylabel('x'' [mrad]');
set(gca, 'linewidth', 2, 'fontsize', 14);
box
print -dpng plot_xxp.png

