figure(1)
clf ; hold on

load emitt_100_0deg_2.dat
plot(T(:,1), T(:,4), 'displayname', '0 deg', 'linewidth', 2);

load emitt_100_8deg_2.dat
plot(T(:,1), T(:,4), 'displayname', '8 deg', 'linewidth', 2);

load emitt_100_16deg_2.dat
plot(T(:,1), T(:,4), 'displayname', '16 deg', 'linewidth', 2);

load emitt_100_32deg_2.dat
plot(T(:,1), T(:,4), 'displayname', '32 deg', 'linewidth', 2);

legend('location', 'northwest')
title ('RF-Track')
xlabel('S [m]');
ylabel('normalized emitt_x [mm.mrad]');

print -dpng plot_emitt_phases.png
