function [TWS, S0] = load_tws(max_field, phase)
    RF_Track;
    T    = load('field_maps/TWS_2.8GHz_84_CELL.SF7');
    S    = T(:,1); % m
    L    = range(S); % m
    S0   = min(S); % m
    S1   = max(S); % m
    freq = 2.7974e9; % Hz
    TR   = T(:,2) * max_field;
    TI   = T(:,3) * max_field;
    Ez   = complex(TR, -TI) ; % V/m
    dS   = S(2) - S(1); % m
    TWS  = RF_FieldMap_1d(Ez, dS, -1, freq, +1);
    TWS.set_smooth(30);
    TWS.set_phid(phase);
end



