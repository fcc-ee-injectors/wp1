close all; clear all

f0 = 2.8; %GHz
c = 299.792458; %[mm/ns] - speed of light

Eacc = 25;  %[MV/m] - Accelerating gradient (active average)
Nc = 84;    % - Number of cells including coupling cells
Lc = c / f0 / 3; %[mm] - cell length
Lbp = 2*Lc; %[mm] - beam pipe length
Ls = Nc*Lc; %[mm] - active length
Vacc = Eacc*1e6*(Ls/1e3); %[V] - acceleration per structure


load('Ez00_z')
dz = z_(2)-z_(1); %[mm]
Vz0 = cumsum(Ez00_.*exp(sqrt(-1)*(2*pi*(f0*z_/c))))*dz/1000; %[V] Vacc on axis

% setting phase to Eacc on crest
Ez00 = Ez00_*Vacc/Vz0(length(Vz0));  

EaccIn = 55;    %[MV/m] input cell gradient
EaccOutUL = 40; %[MV/m] output cell gradient unloaded

% making unloaded field map
Ez00UL = Ez00/Eacc.*(EaccIn+(z_-Lbp)*(EaccOutUL-EaccIn)/Ls); %unloaded map
% making beam field map 
Ez00BL = Ez00/Eacc.*((z_-Lbp)*(EaccIn+EaccOutUL-2*Eacc)/Ls); % beam loading map

% Ez Field maps are ready

Qb = 0.325; %[nC] bunch charge 
fb = 3;     %[GHz] bunch frequency
Ib = Qb*fb  %[A] beam current

figure(3)
plot(z_,abs(Ez00)/1e6,'b-',...
     z_,abs(Ez00UL)/1e6,'r-',...
     z_,abs(Ez00BL)/1e6,'k-',...
     z_,abs(Ez00UL-Ez00BL)/1e6,'m-'); hold on; grid on
xlabel('z [mm]'); ylabel('|Ez| [MV/m]')
legend(sprintf('ConstG: %d MV/m',Eacc), 'Unloaded',...
       sprintf('Beam: Ib = %3.3f A',Ib),...
       sprintf('Loaded: %d MV/m',Eacc))

 save -mat TW_field.mat z_ Ez00UL Ez00BL
