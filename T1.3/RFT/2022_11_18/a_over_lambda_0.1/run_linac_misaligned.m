addpath('scripts/');

do_plots = false;

%% Load RF-Track
RF_Track;

Nseeds = 100;

for Phid = [ 8 ] % deg

    %% Bunch parameters
    mass = RF_Track.electronmass; % MeV/c^2
    charge = -1; % single-particle charge, in units of e
    population = 5.5 * RF_Track.nC; % number of real particles per bunch
    P0 = 1540; % MeV/c, initial reference momentum
    P1 = 6000; % MeV/c, final reference momentum
    emitt_initial = 5; % mm.mrad, initial normalized emittance
    sigma_z = 1; % mm
    Pspread = 0.1; % percent, momentum spread

    %% Misalignment
    QD_x = 50e-6 % m rms
    QD_y = 50e-6 % m rms
    SD_x = 100e-6 % m rms
    SD_y = 100e-6 % m rms

    %% Accelerating structure
    Grad = 25; % MV/m
               % Phid = 0; % deg, rf phase
    freq = 2.8; % Ghz

    % short-range wakes and cell length
    Lstr = 3; % m, structure length
    a_over_lambda = 0.1; %
    lambda = 299.792458 / freq; % mm, RF wavelength
    ph_adv = 2*pi/3; % radian, phase advance per cell
    l = lambda * ph_adv / (2*pi); % mm, cell length
    a = a_over_lambda * lambda; % mm, iris aperture
    g = l - 3; % mm, gap length
    SRWF = ShortRangeWakefield(a/1e3, g/1e3, l/1e3);

    %% Setup the lattice
    Lstr = 3; % m
    n_cells = 3*floor(ceil(Lstr*1e3 / l)/3); % number of cells, negative sign indicates a start from the beginning of the cell
    RF = TW_Structure(-Grad*1e6, 0, freq*1e9, ph_adv, n_cells);
    RF.set_phid(Phid);
    RF.set_odeint_algorithm('leapfrog');
    RF.set_odeint_epsabs(1e-3);
    RF.set_nsteps(100);
    RF.set_cfx_nsteps(10);
    RF.add_collective_effect(SRWF);

    %% Gets the momentum gain per struture
    P = Bunch6d(mass, population, charge, [ 0 0 0 0 0 P0 ]);
    L = Lattice();
    L.append(RF);
    L.autophase(P);
    Pgain = L.track(P){1}.Pc - P0

    %% FODO cell paramters
    Lstr = RF.get_length();
    Nstr = 2; % number of structures per FODO cell
    Lquad = 0.25; % m
    Ldrift = 0.25; % m
    Lcell = Nstr*Lstr + 2*Lquad + 4*Ldrift; % m, two structures
    mud = 90; % deg, phase advance
    k1 = sind(mud/2) / (Lcell/4) / Lquad; % 1/m^2

    %% Define Twiss parameters
    Twiss = Bunch6d_twiss();
    Twiss.emitt_x = emitt_initial; % mm.mrad, normalized emittances
    Twiss.emitt_y = emitt_initial; % mm.mrad
    Twiss.beta_x = Lcell * (1 + sind(mud/2)) / sind(mud); % m
    Twiss.beta_y = Lcell * (1 - sind(mud/2)) / sind(mud); % m
    Twiss.sigma_t = sigma_z; % mm/c
    Twiss.sigma_d = Pspread * 10; % permill

    %% Create the bunch
    B0 = Bunch6d(mass, population, charge, P0, Twiss, 10000);

    %
    T = [];
    E = [];
    for seed=1:Nseeds
        %
        Pref = P0;
        Qf = Quadrupole(Lquad/2, Pref / charge, k1);
        Qf.set_offsets(QD_x*randn, QD_y*randn, 0.0);
        L = Lattice();
        L.append(Qf); % half quadrupole
        while Pref<P1
            %%
            L.append(Drift(Ldrift));
            RF.set_offsets(SD_x*randn, SD_y*randn, 0.0);
            L.append(RF);
            Pref += Pgain; % MeV/c
            L.append(Drift(Ldrift));
            Qd = Quadrupole(Lquad, Pref / charge, -k1);
            Qd.set_offsets(QD_x*randn, QD_y*randn, 0.0);
            L.append(Qd);
            %%
            L.append(Drift(Ldrift));
            RF.set_offsets(SD_x*randn, SD_y*randn, 0.0);
            L.append(RF);
            Pref += Pgain; % MeV/c
            L.append(Drift(Ldrift));
            Qf = Quadrupole(Lquad, Pref / charge, k1);
            Qf.set_offsets(QD_x*randn, QD_y*randn, 0.0);
            L.append(Qf);
        end
        Pfinal = Pref

        %% Perform tracking
        tic
        Pmax = L.autophase(P)
        B1 = L.track(B0);
        toc
        
        if do_plots
            M1 = B1.get_phase_space('%x %xp %y %yp %dt %P');
            subplot(1,3,1)
            scatter(M1(:,1), M1(:,2), '.');
            xlabel('x [mm]');
            ylabel('x'' [mrad]');
            subplot(1,3,2)
            scatter(M1(:,5), M1(:,1), '.');
            xlabel('dt [mm/c]');
            ylabel('x [mm]');
            subplot(1,3,3)
            scatter(M1(:,5), M1(:,6), '.');
            xlabel('dt [mm/c]');
            ylabel('P [MeV/c]');
            drawnow
        end
        
        %% Retrieve the Twiss plot and the phase space
        if seed==1
            T = L.get_transport_table('%S %beta_x %beta_y %emitt_x %emitt_y %mean_Pz');
        else
            T += L.get_transport_table('%S %beta_x %beta_y %emitt_x %emitt_y %mean_Pz');
        end
        I1 = B1.get_info();

        disp([ I1.emitt_x I1.emitt_y ]);
        
        E = [ E ; I1.emitt_x I1.emitt_y ];

    end
    T /= Nseeds;
    
    save('-text', sprintf('emitt_%d_%ddeg.dat', Nseeds, Phid), 'E', 'T');

    figure(1)
    clf ; hold on
    if 1 % max(E(:,1)) > max(E(:,2))
        hist(E(:,1), 20, 'b');
        hist(E(:,2), 20, 'r');
        lgnd = legend({ '\epsilon_x ', '\epsilon_y ' });
    else
        hist(E(:,2), 20, 'r');
        hist(E(:,1), 20, 'b');
        lgnd = legend({ '\epsilon_y ', '\epsilon_x ' });
    end    
    xlabel('\epsilon [mm.mrad]');
    ylabel('counts');
    if Phid != 0
        xlim([ 5 50 ]);
    else
        xlim([ 5 15 ]);
    end
    legend boxoff
    legend boxon
    set(lgnd, 'fontsize', 14);
    set(gca, 'linewidth', 2, 'fontsize', 14);
    title('a/\lambda = 0.1');
    print('-dpng',  sprintf('plot_emitt_%d_%ddeg_hist.png', Nseeds, Phid));
    
    figure(2)
    clf
    hold on
    plot(T(:,1), T(:,4), 'b-*', 'linewidth', 2);
    plot(T(:,1), T(:,5), 'r-*', 'linewidth', 2);
    lgnd = legend({ '\epsilon_x ', '\epsilon_y ' });
    xlabel('S [m]');
    ylabel('\epsilon [mm.mrad]');
    if Phid == 0
        ylim([ 4.8 7 ]);
    else
        ylim([ 4.8 15 ]);
    end
    legend boxoff
    legend boxon
    set(lgnd, 'fontsize', 14);
    set(gca, 'linewidth', 2, 'fontsize', 14);
    box
    drawnow;
    title('a/\lambda = 0.1');
    print('-dpng',  sprintf('plot_emitt_%d_%ddeg.png', Nseeds, Phid));
    
end
