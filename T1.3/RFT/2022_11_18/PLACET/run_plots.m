figure(1)
clf ; hold on

load eLinac_emitt_phid0_n_100_5.5nC.dat 
plot(E0(:,1), E0(:,2)/10, 'displayname', '0 deg', 'linewidth', 2);

load eLinac_emitt_phid8_n_100_5.5nC.dat 
plot(E0(:,1), E0(:,2)/10, 'displayname', '8 deg', 'linewidth', 2);

load eLinac_emitt_phid16_n_100_5.5nC.dat 
plot(E0(:,1), E0(:,2)/10, 'displayname', '16 deg', 'linewidth', 2);

load eLinac_emitt_phid32_n_100_5.5nC.dat 
plot(E0(:,1), E0(:,2)/10, 'displayname', '32 deg', 'linewidth', 2);

legend('location', 'northwest')
title ('PLACET')
xlabel('S [m]');
ylabel('normalized emitt_x [mm.mrad]');

print -dpng plot_emitt_phases.png
