set RF_units 30

set PI 3.141592653589793

set CAV_gradient [expr 0.025 / cos($phi_rf / 180.0 * $PI) ]; # GV/m
set CAV_freq 2.8   ; # GHz
set CAV_len 3        ; # m
set CAV_phase $phi_rf ; # deg
set Q_len  0.25         ; # m

set Ldrift 0.25

set FODO_mu [expr 90.0 * $PI / 180.0 ]
set FODO_len [expr 2.0 * $Q_len + 2.0 * $CAV_len + 4.0 * $Ldrift]

set Qf_k1l [expr  4.0 * sin($FODO_mu / 2.0) / $FODO_len]
set Qd_k1l [expr -4.0 * sin($FODO_mu / 2.0) / $FODO_len]

array set FODO_twiss [MatchFodo -l1 $Q_len -l2 $Q_len -K1 $Qf_k1l -K2 $Qd_k1l -L [expr $FODO_len / 2.0 ] ]

Girder
SetReferenceEnergy $e0
Bpm
Dipole
Quadrupole -len [expr $Q_len / 2.0] -strength [expr $Qf_k1l * $e0 / 2.0]
Bpm
for {set i 0} {$i < $RF_units} {incr i} {
    Drift -length $Ldrift
    Cavity -length $CAV_len -gradient $CAV_gradient -frequency $CAV_freq -phase $CAV_phase
    set e0 [expr $e0 + $CAV_gradient * $CAV_len * cos($CAV_phase * $PI / 180.0) ]
    SetReferenceEnergy $e0
    Drift -length $Ldrift

    Dipole
    Quadrupole -len [expr $Q_len] -strength [expr $Qd_k1l * $e0]
    Bpm
    
    Drift -length $Ldrift
    Cavity -length $CAV_len -gradient $CAV_gradient -frequency $CAV_freq -phase $CAV_phase
    set e0 [expr $e0 + $CAV_gradient * $CAV_len * cos($CAV_phase * $PI / 180.0) ]
    SetReferenceEnergy $e0
    Drift -length $Ldrift

    Dipole
    Quadrupole -len [expr $Q_len] -strength [expr $Qf_k1l * $e0]
    Bpm
}

puts "Efinal = $e0"
