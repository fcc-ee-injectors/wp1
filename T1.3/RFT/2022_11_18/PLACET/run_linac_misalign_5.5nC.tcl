set scripts Common

set basename eLinac

# define the wakefields
source $scripts/wake_init_lr.tcl
source $scripts/wake_init_sr.tcl

set e0 1.54
set e_initial $e0

# BNS damping
set phi_rf 8

source ${basename}_lattice.tcl
BeamlineSet -name "$basename"

foreach {key value} [array get FODO_twiss] {
    if [string match "beta*" $key ] {
	puts "$key => $value"
    }
}

set n_total 32768
set n_slice 64
set n 512

set match(beta_x) $FODO_twiss(beta_x)
set match(beta_y) $FODO_twiss(beta_y)
set match(alpha_x) $FODO_twiss(alpha_x)
set match(alpha_y) $FODO_twiss(alpha_y)
set match(emitt_x) 50 ; # 10^-7 m
set match(emitt_y) 50 ; # 10^-7 m
set match(phase) 0.0
set charge [expr 5.5 * 6241509074.460763 ] ; # e+, 1 nC = 6241509074.460763 e

set match(sigma_z) 1000 ; # um
set match(e_spread) 0.1 ; # percent

source $scripts/make_beam.tcl

make_beam_many beam0 $n_slice $n

FirstOrder 1

Octave {
    AI = placet_get_number_list("$basename", "cavity");
    placet_element_set_attribute("$basename", AI, "six_dim", true);
    placet_element_set_attribute("$basename", AI, "short_range_wake", "SPARC_cav_SR");
    placet_element_set_attribute("$basename", AI, "phase", $phi_rf);
}

SetReferenceEnergy -beamline $basename -beam beam0

Octave {
    if exist('R_${basename}_5.5nC.dat', "file")
    load 'R_${basename}_5.5nC.dat';
    else
    Response.Cx = placet_get_number_list("${basename}", "dipole");
    Response.Cy = placet_get_number_list("${basename}", "dipole");
    Response.Bpms = placet_get_number_list("${basename}", "bpm");

    # picks all correctors preceeding the last bpm, and all bpms following the first corrector
    Response.Cx = Response.Cx(Response.Cx < Response.Bpms(end));
    Response.Cy = Response.Cy(Response.Cy < Response.Bpms(end));
    Response.Bpms = Response.Bpms(Response.Bpms > max(Response.Cx(1), Response.Cy(1)));

    # gets the energy at each corrector
    Response.Ex = placet_element_get_attribute("${basename}", Response.Cx, "e0");
    Response.Ey = placet_element_get_attribute("${basename}", Response.Cy, "e0");
    
    # compute response matrices
    placet_test_no_correction("${basename}", "beam0", "Zero");
    Response.B0 = placet_get_bpm_readings("${basename}", Response.Bpms);
    Response.R0x = placet_get_response_matrix_attribute ("${basename}", "beam0", Response.Bpms, "x", Response.Cx, "strength_x", "Zero");
    Response.R0y = placet_get_response_matrix_attribute ("${basename}", "beam0", Response.Bpms, "y", Response.Cy, "strength_y", "Zero");

    save -text 'R_${basename}_5.5nC.dat' Response;
    end
}

set nm 100
set nbins 1
set noverlap 0

set beta0 0

set sigmaq  50.0
set sigmarf 100.0
set bpmres 10.0

proc my_survey {} {
    global machine sigmaq sigmarf bpmres basename
    Octave {
	randn("seed", $machine * 1e4);
	AI = placet_get_number_list("${basename}", "cavity");
	BI = placet_get_number_list("${basename}", "bpm");
	DI = placet_get_number_list("${basename}", "dipole");
	QI = placet_get_number_list("${basename}", "quadrupole");
	placet_element_set_attribute("${basename}", DI, "strength_x", 0.0);
	placet_element_set_attribute("${basename}", DI, "strength_y", 0.0);
	placet_element_set_attribute("${basename}", BI, "resolution", $bpmres);
	placet_element_set_attribute("${basename}", AI, "x", randn(size(AI)) * $sigmarf);
	placet_element_set_attribute("${basename}", AI, "y", randn(size(AI)) * $sigmarf);
	placet_element_set_attribute("${basename}", BI, "x", randn(size(BI)) * $sigmaq);
	placet_element_set_attribute("${basename}", BI, "y", randn(size(BI)) * $sigmaq);
	placet_element_set_attribute("${basename}", QI, "x", randn(size(QI)) * $sigmaq);
	placet_element_set_attribute("${basename}", QI, "y", randn(size(QI)) * $sigmaq);
    }
}

Octave {
    Emitt_hist=zeros($nm, 2);
    Bpm0X_hist=zeros($nm, length(Response.Bpms));
    Bpm0Y_hist=zeros($nm, length(Response.Bpms));
}

Octave {
    [E,B] = placet_test_no_correction("${basename}", "beam0", "Zero");
}

# Binning
Octave {
    nBpms = length(Response.Bpms);
    Blen = nBpms / ($nbins * (1 - $noverlap) + $noverlap);
    for i=1:$nbins
      Bmin = floor((i - 1) * Blen - (i - 1) * Blen * $noverlap) + 1;
      Bmax = floor((i)     * Blen - (i - 1) * Blen * $noverlap);
      Cxmin = find(Response.Cx < Response.Bpms(Bmin))(end);
      Cxmax = find(Response.Cx < Response.Bpms(Bmax))(end);
      Cymin = find(Response.Cy < Response.Bpms(Bmin))(end);
      Cymax = find(Response.Cy < Response.Bpms(Bmax))(end);
      Bins(i).Bpms = Bmin:Bmax;
      Bins(i).Cx = Cxmin:Cxmax;
      Bins(i).Cy = Cymin:Cymax;
    end
    printf("Each bin contains approx %g bpms.\n", round(Blen));
}

Octave {
    E0=0;
    Bpm0=0;
}

for {set machine 1} {$machine <= $nm} {incr machine} {
    
    puts "MACHINE: $machine/$nm"
    
    my_survey
    
    Octave {
	disp("TRACKING...");
	[E,B] = placet_test_no_correction("$basename", "beam0", "None");
	if $machine == 1
	E0 = E;
	else
	E0 += E;
	end
	Emitt_hist($machine, :) = E(end,[ 2 6 ]);
	SizeX_hist($machine, 1) = std(B(:,2));
	SizeY_hist($machine, 1) = std(B(:,3));
	Bpm0 = placet_get_bpm_readings("${basename}", Response.Bpms);
	Bpm0X_hist($machine, :) = Bpm0(:,1)';
	Bpm0Y_hist($machine, :) = Bpm0(:,2)';
    }
}

Octave {
    E0 /= $nm;
    save -text ${basename}_emitt_phid${phi_rf}_n_${nm}_5.5nC.dat E0
    save -text ${basename}_emitt_phid${phi_rf}_n_${nm}_5.5nC_hist.dat Emitt_hist
}
