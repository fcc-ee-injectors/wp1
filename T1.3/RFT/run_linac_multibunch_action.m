%% Load RF-Track
RF_Track;

%% Bunch parameters
mass = RF_Track.electronmass; % MeV/c^2
charge = -1; % single-particle charge, in units of e
population = 3.2 * RF_Track.nC; % number of real particles per bunch
P0 = 1540; % MeV/c, initial reference momentum
P1 = 6000; % MeV/c, final reference momentum
emitt_initial = 10; % mm.mrad, initial normalized emittance
beta_average = 20; % m;
sigma_z = 3; % mm
Pspread = 0.1; % percent, momentum spread
% multi-bunch
S_bunch = 17.5 * RF_Track.ns / 1e3; % m, intra-bunch separation
N_bunches = 50;

%% Accelerating structure
Lstr = 3; % m
Grad = 25; % MV/m
Phid = 0; % deg, rf phase
freq = 3; % Ghz

%% FODO cell paramters
Nstr = 8; % number of structures per FODO cell
Lcell = Nstr*Lstr; % m, two structures
Lquad = 0; % m, thin lens
Ldrift = Lcell/2 - Lquad; % m
mu = 90; % deg, phase advance
k1L = sind(mu/2) / (Lcell/4); % 1/m
beta_average = 0.5 * Lcell * (cotd(0.5*mud) + 2/3*tand(0.5*mud)) % m

% beam size
sigma_x = sqrt(beta_average * emitt_initial * mass / P0) % mm
sigma_xp = sqrt(emitt_initial * mass / P0 / beta_average) % mrad

%% Long-range wakefield
Kick_t = 10; % V/pC/m/mm
Kick_l = 0; % V/pC/m

Wt = [ 0 Kick_t 0 ]; % V/pC/m/mm
Wl = [ 0 Kick_l 0 ]; % V/pC/m
WF = Wakefield_1d_LINT(Wt, Wl, S_bunch, 120);

%% Accelerating field
% RF = TW_Structure(-Grad*1e6, 0, freq*1e9, ph_adv, -n_cells);
% RF.set_phid(Phid);
RF = Drift(Nstr*Lstr/2);
RF.set_static_Efield(0, 0, -Grad*1e6);
RF.set_odeint_algorithm('rk2');
RF.set_odeint_epsabs(1e-6);
RF.set_cfx_nsteps(20);
RF.add_collective_effect(WF);

% Setup the lattice
L = Lattice();
Pref = P0;
while Pref <= P1
    Pref
    B_rho = Pref / charge; % MV/c, reference rigidity
    L.append(Quadrupole(Lquad/2, k1L/2 * B_rho));
    L.append(RF);
    Pref += Grad * RF.get_length(); % MeV/c
    B_rho = Pref / charge; % MV/c, reference rigidity
    L.append(Quadrupole(Lquad, -k1L * B_rho));
    L.append(RF);
    Pref += Grad * RF.get_length(); % MeV/c
    B_rho = Pref / charge; % MV/c, reference rigidity
    L.append(Quadrupole(Lquad/2, k1L/2 * B_rho));
end

%% Create the beam
Angled_range = linspace(0, 360, 36); % deg
clear T
iA = 1;
for Angled = Angled_range
    
    printf('Angle %d/%d\n', iA, length(Angled_range));
    
    Dx  = 0.1 * sigma_x  * cosd(Angled); % mm
    DPx = 0.1 * sigma_xp * sind(Angled); % MeV/c
    
    M0_ = [ Dx DPx/P0 0 0 0 P0 ];
    M0 = [];
    for n=1:N_bunches
        M0 = [ M0 ; M0_ ];
        M0_(:,5) += S_bunch*1e3; % mm/c
    end
    
    B0 = Bunch6d(RF_Track.electronmass, population * N_bunches, -1, M0);
    
    %% Perform tracking
    B1 = L.track(B0);
    
    %% Retrieve the phase space coordinate
    M0 = B0.get_phase_space('%x %Px');
    M1 = B1.get_phase_space('%x %Px');
    
    % prepares data structure
    for iB=1:N_bunches
        A0_{iB}(iA,1) = M0(iB,1);
        A0_{iB}(iA,2) = M0(iB,2);
        A1_{iB}(iA,1) = M1(iB,1);
        A1_{iB}(iA,2) = M1(iB,2);
    end        
        
    iA++;
end

clear A0 A1
for iB = 1:N_bunches
    A0(iB) = polyarea(A0_{iB}(:,1), A0_{iB}(:,2));
    A1(iB) = polyarea(A1_{iB}(:,1), A1_{iB}(:,2));
end

%% Make plots
figure(1)
clf
hold on
plot(A1 ./ A0, 'r-*', 'linewidth', 2);
xlabel('bunch #');
ylabel('A_{final} / A_{initial}');
print -dpng plot_multi_bunch_action.png
