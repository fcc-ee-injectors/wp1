%% Load RF-Track
RF_Track;

%% Bunch parameters
mass = RF_Track.electronmass; % MeV/c^2
charge = -1; % single-particle charge, in units of e
population = 3.2 * RF_Track.nC; % number of real particles per bunch
P0 = 1540; % MeV/c, initial reference momentum
P1 = 6000; % MeV/c, final reference momentum
emitt_initial = 10; % mm.mrad, initial normalized emittance
sigma_z = 3; % mm
Pspread = 0.1; % percent, momentum spread
% multi-bunch
S_bunch = 17.5 * RF_Track.ns / 1e3; % m, intra-bunch separation
N_bunches = 50;

%% Accelerating structure
Lstr = 3; % m
Grad = 25; % MV/m
Phid = 0; % deg, rf phase
freq = 3; % Ghz

%% FODO cell paramters
Nstr = 8; % number of structures per FODO cell
Lcell = Nstr*Lstr; % m, two structures
Lquad = 0; % m, thin lens
Ldrift = Lcell/2 - Lquad; % m
mu = 90; % deg, phase advance
k1L = sind(mu/2) / (Lcell/4); % 1/m

%% Long-range wakefield
Kick_t = 1; % V/pC/m/mm
Kick_l = 100; % V/pC/m

Wt = [ 0 Kick_t 0 ]; % V/pC/m/mm
Wl = [ 0.5*Kick_l Kick_l 0 ]; % V/pC/m

WF = Wakefield_1d(Wt, Wl, S_bunch);

%% Accelerating field
% RF = TW_Structure(-Grad*1e6, 0, freq*1e9, ph_adv, -n_cells);
% RF.set_phid(Phid);
RF = Drift(Nstr*Lstr/2);
RF.set_static_Efield(0, 0, -Grad*1e6);
RF.set_odeint_algorithm('rk2');
RF.set_odeint_epsabs(1e-6);
RF.set_cfx_nsteps(100);
RF.add_collective_effect(WF);

% Setup the lattice
L = Lattice();
Pref = P0;
while Pref <= P1
    Pref
    B_rho = Pref / charge; % MV/c, reference rigidity
    L.append(Quadrupole(Lquad/2, k1L * B_rho / 2));
    L.append(RF);
    Pref += Grad * RF.get_length(); % MeV/c
    B_rho = Pref / charge; % MV/c, reference rigidity
    L.append(Quadrupole(Lquad, -k1L * B_rho));
    L.append(RF);
    Pref += Grad * RF.get_length(); % MeV/c
    B_rho = Pref / charge; % MV/c, reference rigidity
    L.append(Quadrupole(Lquad/2, k1L * B_rho / 2));
end

%% Create the beam

DX = 10; % mm

M0_ = [ DX 0 0 0 0 P0 ];
M0 = [];
for n=1:N_bunches
    M0 = [ M0 ; M0_ ];
    M0_(:,5) += S_bunch*1e3; % mm/c
end

B0 = Bunch6d(RF_Track.electronmass, population * N_bunches, -1, M0);

%% Perform tracking
B1 = L.track(B0);
M1 = B1.get_phase_space();

%% Retrieve the Twiss plot and the phase space
T = L.get_transport_table('%S %mean_x %mean_y %mean_Pz');

%% Make plots
figure(1)
clf
hold on
plot(T(:,1), T(:,2), 'b-*', 'linewidth', 2);
plot(T(:,1), T(:,3), 'r-*', 'linewidth', 2);
lgnd = legend({ '<x> ', '<y> ' });
xlabel('s [m]');
ylabel('[mm]');
legend boxoff
legend boxon
set(lgnd, 'fontsize', 14);
set(gca, 'linewidth', 2, 'fontsize', 14);
box
print -dpng plot_orbit.png

figure(2)
clf
hold on
plot(M1(:,1), 'b-*', 'linewidth', 2);
plot(M1(:,2), 'r-*', 'linewidth', 2);
lgnd = legend({ '<x> [mm]', '<x''> [mrad] ' });
xlabel('bunch #');
ylabel('-');
legend boxoff
legend boxon
set(lgnd, 'fontsize', 14);
set(gca, 'linewidth', 2, 'fontsize', 14);
box
print -dpng plot_train.png

figure(3)
clf
plot(M1(:,6), 'r-*', 'linewidth', 2);
axis([ 0 N_bunches 3000 7000 ] )
lgnd = legend({ '<P>' });
xlabel('bunch #');
ylabel('P [MeV/c]');
legend boxoff
legend boxon
set(lgnd, 'fontsize', 14);
print -dpng plot_train_P.png

figure(4)
clf
hold on
plot(T(:,1), T(:,4), 'r-*', 'linewidth', 2);
xlabel('S [m]');
ylabel('Pz [MeV/c]');
legend boxoff
legend boxon
set(lgnd, 'fontsize', 14);
set(gca, 'linewidth', 2, 'fontsize', 14);
box
print -dpng plot_Pz_train_average.png
