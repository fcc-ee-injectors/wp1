%% Load RF-Track
RF_Track;

%% Bunch parameters
mass = RF_Track.electronmass; % MeV/c^2
charge = -1; % single-particle charge, in units of e
population = 2 * RF_Track.nC; % number of real particles per bunch
P0 = 1540; % MeV/c, initial reference momentum
P1 = 6000; % MeV/c, final reference momentum
emitt_initial = 10; % mm.mrad, initial normalized emittance
sigma_z = 3; % mm
Pspread = 0.1; % percent, momentum spread
               % multi-bunch
S_bunch = 17.5 * RF_Track.ns / 1e3; % m, intra-bunch separation
N_bunches = 5;

%% Accelerating structure
Lstr = 3; % m
Grad = 25; % MV/m
Phid = 0; % deg, rf phase
freq = 3; % Ghz

%% FODO cell paramters
Nstr = 8; % number of structures per FODO cell
Lcell = Nstr*Lstr; % m, two structures
Lquad = 0; % m, thin lens
Ldrift = Lcell/2 - Lquad; % m
mud = 90; % deg, phase advance
k1L = sind(mud/2) / (Lcell/4); % 1/m
beta_average = 0.5 * Lcell * (cotd(0.5*mud) + 2/3*tand(0.5*mud)) % m

sigma_x = sqrt(beta_average * emitt_initial * mass / P0) % mm
sigma_xp = sqrt(emitt_initial * mass / P0 / beta_average) % mrad
figure(1)
clf ; hold on ;

%% Long-range wakefield
Kick_t = -1; % V/pC/m/mm
Kick_l =  0; % V/pC/m

Wt = [ 0 Kick_t 0 ]; % V/pC/m/mm
Wl = [ 0 Kick_l 0 ]; % V/pC/m
WF = Wakefield_1d_LINT(Wt, Wl, S_bunch, 1000);

%% Accelerating field
% RF = TW_Structure(-Grad*1e6, 0, freq*1e9, ph_adv, -n_cells);
% RF.set_phid(Phid);
RF = Drift(Nstr*Lstr/2);
RF.set_static_Efield(0, 0, -Grad*1e6);
RF.set_odeint_algorithm('rk2');
RF.set_odeint_epsabs(1e-6);
RF.set_nsteps(1000);
RF.set_cfx_nsteps(20);
RF.add_collective_effect(WF);

S_axis = [ 0 ];
Orbits = zeros(16, N_bunches);
Angles = zeros(16, N_bunches);

for N_elements = 1:16
    % Setup the lattice
    L = Lattice();
    Pref = P0;
    Ncells = 0;
    B_rho = Pref / charge; % MV/c, reference rigidity
    for element = 1:N_elements
        if rem(element,2) == 1
            L.append(Quadrupole(Lquad/2, k1L/2 * B_rho));
            L.append(RF);
            Pref += Grad * RF.get_length(); % MeV/c
            B_rho = Pref / charge; % MV/c, reference rigidity
            L.append(Quadrupole(Lquad/2, -k1L/2 * B_rho));
        else
            L.append(Quadrupole(Lquad/2, -k1L/2 * B_rho));
            L.append(RF);
            Pref += Grad * RF.get_length(); % MeV/c
            B_rho = Pref / charge; % MV/c, reference rigidity
            L.append(Quadrupole(Lquad/2, k1L/2 * B_rho));
        end
    end
    
    %% Create the beam
    Angled = 0
    
    Dx  = 0.1 * sigma_x  * cosd(Angled); % mm
    Dxp = 0.1 * sigma_xp * sind(Angled); % MeV/c
    
    M0_ = [ Dx Dxp 0 0 0 P0 ];
    M0 = [];
    for n=1:N_bunches
        M0 = [ M0 ; M0_ ];
        M0_(:,5) += S_bunch*1e3; % mm/c
    end
    
    B0 = Bunch6d(RF_Track.electronmass, population * N_bunches, -1, M0);
    
    %% Perform tracking
    B1 = L.track(B0);
    
    %% Retrieve the phase space coordinate
    M1 = B1.get_phase_space('%S %x %Px');
    
    
    S_axis = [ S_axis ; mean(M1(:,1)) ];
    Orbits(1, :) = M0(:,1)';
    Orbits(N_elements+1, :) = M1(:,2)';

    Angles(1, :) = M0(:,2)';
    Angles(N_elements+1, :) = M1(:,3)';
    
end


figure(1);
clf
hold on
Lgnd = {};
for i=1:5
    plot(S_axis / 1e3, Orbits(:,i), 'linewidth', 2);
    xlabel('S [m]');
    ylabel('X [mm]')
    axis([ 0 200 -0.03 0.03])
    Lgnd{i} = sprintf('bunch #%d  ', i);
    legend(Lgnd);
    print('-dpng', sprintf('plot_track_%d.png', i));
end

figure(2);
clf
hold on
Lgnd = {};
for i=1:5
    plot(S_axis / 1e3, Angles(:,i), 'linewidth', 2);
    xlabel('S [m]');
    ylabel('P_x [MeV/c]')
    axis([ 0 200 -0.03 0.03])
    Lgnd{i} = sprintf('bunch #%d  ', i);
    legend(Lgnd);
    print('-dpng', sprintf('plot_track_angle_%d.png', i));
end

system('for i in *png;  do anytopnm $i | pnmcrop -sides | pnmtopng > tmp.png && mv tmp.png $i ; done');