%% Load RF-Track
RF_Track;

%% Bunch parameters
mass = RF_Track.electronmass; % MeV/c^2
charge = -1; % single-particle charge, in units of e
population = 3.4 * RF_Track.nC; % number of real particles per bunch
P0 = 1540; % MeV/c, initial reference momentum
P1 = 6000; % MeV/c, final reference momentum
emitt_initial = 10; % mm.mrad, initial normalized emittance
sigma_z = 3; % mm
Pspread = 0.1; % percent, momentum spread
               % multi-bunch
S_bunch = 17.5 * RF_Track.ns / 1e3; % m, intra-bunch separation
N_bunches = 10;

%% Accelerating structure
Lstr = 3; % m
Grad = 25; % MV/m
Phid = 0; % deg, rf phase
freq = 3; % Ghz

%% FODO cell paramters
Nstr = 8; % number of structures per FODO cell
Lcell = Nstr*Lstr; % m, two structures
Lquad = 0; % m, thin lens
Ldrift = Lcell/2 - Lquad; % m
mud = 90; % deg, phase advance
k1L = sind(mud/2) / (Lcell/4); % 1/m
beta_average = 0.5 * Lcell * (cotd(0.5*mud) + 2/3*tand(0.5*mud)) % m

sigma_x = sqrt(beta_average * emitt_initial * mass / P0) % mm
sigma_xp = sqrt(emitt_initial * mass / P0 / beta_average) % mrad
figure(1)
clf ; hold on ;
Lgnd = {};
iLgnd = 1;
for pattern = ((2**N_bunches)-1):-1:1

    P_ = fliplr(dec2bin(pattern, N_bunches));
    P = zeros(N_bunches,1);
    for i=1:N_bunches
        P(i) = str2num(P_(i));
    end
    printf('pattern = %d (%s)\n', pattern, P_); 

    %% Long-range wakefield
    Kick_t = 0; % V/pC/m/mm
    Kick_l = 100; % V/pC/m

    Wt = [ 0 -Kick_t 0 ]; % V/pC/m/mm
    Wl = [ 0  Kick_l 0 ]; % V/pC/m
    WF = Wakefield_1d_LINT(Wt, Wl, S_bunch, 1000);

    %% Accelerating field
    % RF = TW_Structure(-Grad*1e6, 0, freq*1e9, ph_adv, -n_cells);
    % RF.set_phid(Phid);
    RF = Drift(Nstr*Lstr/2);
    RF.set_static_Efield(0, 0, -Grad*1e6);
    RF.set_odeint_algorithm('rk2');
    RF.set_odeint_epsabs(1e-6);
    RF.set_nsteps(100);
    RF.set_cfx_nsteps(20);
    RF.add_collective_effect(WF);

    % Setup the lattice
    L = Lattice();
    Pref = P0;
    while Pref <= P1
        Pref
        B_rho = Pref / charge; % MV/c, reference rigidity
        L.append(Quadrupole(Lquad/2, k1L/2 * B_rho));
        L.append(RF);
        Pref += Grad * RF.get_length(); % MeV/c
        B_rho = Pref / charge; % MV/c, reference rigidity
        L.append(Quadrupole(Lquad, -k1L * B_rho));
        L.append(RF);
        Pref += Grad * RF.get_length(); % MeV/c
        B_rho = Pref / charge; % MV/c, reference rigidity
        L.append(Quadrupole(Lquad/2, k1L/2 * B_rho));
    end

    %% Create the beam
    N = (1:N_bunches)';
    O = zeros(N_bunches,1);
    I = ones(N_bunches,1);
    R = randn(N_bunches,1);
    M0 = [ O O O O (N-1)*S_bunch*1e3 I*P0 I*RF_Track.electronmass -I population*P ];
    B0 = Bunch6d(M0);
                
    %% Perform tracking
    B1 = L.track(B0);
    
    %% Retrieve the phase space coordinate
    M1 = B1.get_phase_space();

    M1(P==0,6) = nan;
        
    %% Make plots
    plot(M1(:,6) / 6340 * 100, '-*', 'linewidth', 2);
    legend(P_);
    grid on
    xlabel('bunch #');
    ylabel('dP / P_{final} [%]');
    axis([ 0 N_bunches+1 95 102 ]);
    drawnow
    if pattern > 1000
        print('-dpng', sprintf('plot_multi_bunch_beam_loading_pattern_%d.png', pattern));
        clf
    end
    if pattern == 1000
        hold on
    end
end

print -dpng plot_multi_bunch_beam_loading_pattern.png

system('for i in *png;  do anytopnm $i | pnmcrop -sides | pnmtopng > tmp.png && mv tmp.png $i ; done');
