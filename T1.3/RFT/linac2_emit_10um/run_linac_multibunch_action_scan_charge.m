%% Load RF-Track
RF_Track;

%% Bunch parameters
mass = RF_Track.electronmass; % MeV/c^2
charge = -1; % single-particle charge, in units of e
population = 3.4 * RF_Track.nC; % number of real particles per bunch
P0 = 1540; % MeV/c, initial reference momentum
P1 = 6000; % MeV/c, final reference momentum
emitt_initial = 10; % mm.mrad, initial normalized emittance
sigma_z = 3; % mm
Pspread = 0.1; % percent, momentum spread
               % multi-bunch
S_bunch = 17.5 * RF_Track.ns / 1e3; % m, intra-bunch separation
N_bunches = 50;

%% Accelerating structure
Lstr = 3; % m
Grad = 25; % MV/m
Phid = 0; % deg, rf phase
freq = 3; % Ghz

%% FODO cell paramters
Nstr = 8; % number of structures per FODO cell
Lcell = Nstr*Lstr; % m, two structures
Lquad = 0; % m, thin lens
Ldrift = Lcell/2 - Lquad; % m
mud = 90; % deg, phase advance
k1L = sind(mud/2) / (Lcell/4); % 1/m
beta_average = 0.5 * Lcell * (cotd(0.5*mud) + 2/3*tand(0.5*mud)) % m

dQ_Q = 0.01; % rms bunch-to-bunch charge variation

sigma_rms = 200e-6; % m, rms structure misalignment

sigma_x = sqrt(beta_average * emitt_initial * mass / P0) % mm
sigma_xp = sqrt(emitt_initial * mass / P0 / beta_average) % mrad
figure(1)
clf ; hold on ;
Lgnd = {};
iLgnd = 1;
for seed = 1:10
    
    %% Long-range wakefield
    Kick_t = 1; % V/pC/m/mm
    Kick_l = 0; % V/pC/m

    Wt = [ 0 -Kick_t 0 ]; % V/pC/m/mm
    Wl = [ 0  Kick_l 0 ]; % V/pC/m
    WF = Wakefield_1d (Wt, Wl, S_bunch, 1000);

    %% Accelerating field
    % RF = TW_Structure(-Grad*1e6, 0, freq*1e9, ph_adv, -n_cells);
    % RF.set_phid(Phid);
    RF = Drift(Nstr*Lstr/2);
    RF.set_static_Efield(0, 0, -Grad*1e6);
    RF.set_odeint_algorithm('rk2');
    RF.set_odeint_epsabs(1e-6);
    RF.set_nsteps(1000);
    RF.set_cfx_nsteps(20);
    RF.add_collective_effect(WF);

    % Setup the lattice
    randn('seed', 123);
    L = Lattice();
    Pref = P0;
    Qf = Quadrupole(Lquad/2, k1L/2 * Pref / charge);
    dx = sigma_rms*randn;
    while Pref <= P1
        Pref
        L.append(Qf, dx, 0, 0);;
        L.append(RF, sigma_rms*randn, 0, 0); Pref += Grad * RF.get_length(); % MeV/c
        Qd = Quadrupole(Lquad, -k1L * Pref / charge);
        L.append(Qd, sigma_rms*randn, 0, 0)
        L.append(RF, sigma_rms*randn, 0, 0); Pref += Grad * RF.get_length(); % MeV/c
        Qf = Quadrupole(Lquad/2, k1L/2 * Pref / charge);
        L.append(Qf, dx = sigma_rms*randn, 0, 0);
    end

    %% Create the beam
    Angled_range = linspace(0, 360, 20); % deg
    clear T
    iA = 1;
    R = rand(N_bunches,1);
    population_ = population.*(1 .- dQ_Q*R)
    for Angled = Angled_range
        
        printf('Angle %d/%d\n', iA, length(Angled_range));
        
        Dx  = 0.1 * sigma_x  * cosd(Angled); % mm
        Dxp = 0.1 * sigma_xp * sind(Angled); % MeV/c
        
        N = (1:N_bunches)';
        O = zeros(N_bunches,1);
        I = ones(N_bunches,1);
        M0 = [ Dx*I Dxp*I O O (N-1)*S_bunch*1e3 I*P0 I*RF_Track.electronmass -I population_ ];
        B0 = Bunch6d(M0);
        
        %% Perform tracking
        B1 = L.track(B0);
        
        %% Retrieve the phase space coordinate
        M0 = B0.get_phase_space('%x %Px');
        M1 = B1.get_phase_space('%x %Px');
        
        % prepares data structure
        for iB=1:N_bunches
            A0_{iB}(iA,1) = M0(iB,1);
            A0_{iB}(iA,2) = M0(iB,2);
            A1_{iB}(iA,1) = M1(iB,1);
            A1_{iB}(iA,2) = M1(iB,2);
        end        
        
        iA++;
    end

    clear A0 A1
    for iB = 1:N_bunches
        A0(iB) = polyarea(A0_{iB}(:,1), A0_{iB}(:,2));
        A1(iB) = polyarea(A1_{iB}(:,1), A1_{iB}(:,2));
    end

    %% Make plots
    plot(A1 ./ A0, '-*', 'linewidth', 2);
    drawnow;
end

xlabel('bunch #');
ylabel('A_{final} / A_{initial}');
grid

print -dpng plot_multi_bunch_action_charge.png
system('for i in plot_multi_bunch_action_charge.png; do anytopnm $i | pnmcrop -sides | pnmtopng > tmp.png && mv tmp.png $i ; done');

figure(2)
clf ; hold on
plot(A0_{1}(:,1), A0_{1}(:,2), 'linewidth', 2);
for i=1:50;  plot(A1_{i}(:,1), A1_{i}(:,2)) ; end
xlabel('X [mm]');
ylabel('P_x [MeV/c]');
print -dpng plot_multi_bunch_action_charge_phase_space.png
system('for i in plot_multi_bunch_action_charge_phase_space.png; do anytopnm $i | pnmcrop -sides | pnmtopng > tmp.png && mv tmp.png $i ; done');

if 0 

figure(2)
clf
box off
hold on
xlabel('X [mm]')
ylabel('P_x [MeV/c]')
plot(A0_{1}(:,1), A0_{1}(:,2), 'o--', 'linewidth', 2)
legend('offset at injection  ')
box off
axis([ -0.04 0.04 -0.006 0.006 ]);
print -dpng plot_injection.png
plot(A1_{1}(:,1), A1_{1}(:,2), 'o--', 'linewidth', 2)
legend('offset at injection  ', 'offset at extraction - 1st bunch   ')
print -dpng plot_extraction_1st.png
plot(A1_{2}(:,1), A1_{2}(:,2), 'o--', 'linewidth', 2)
legend('offset at injection  ', 'offset at extraction - 1st bunch', 'offset at extraction - 2nd bunch   ')
print -dpng plot_extraction_2nd.png
plot(A1_{10}(:,1), A1_{10}(:,2), 'o--', 'linewidth', 2)
legend('offset at injection  ', 'offset at extraction - 1st bunch', ...
       'offset at extraction - 2nd bunch   ', ...
       'offset at extraction - 10th bunch  ')
print -dpng plot_extraction_10th.png

end