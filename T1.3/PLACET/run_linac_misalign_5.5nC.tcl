set scripts Common

set basename eLinac

# define the wakefields
source $scripts/wake_init_lr.tcl
source $scripts/wake_init_sr.tcl

set e0 1.54
set e_initial $e0

source ${basename}_lattice.tcl
BeamlineSet -name "$basename"

foreach {key value} [array get FODO_twiss] {
    if [string match "beta*" $key ] {
	puts "$key => $value"
    }
}

set n_total 32768
set n_slice 64
set n 512

set match(beta_x) $FODO_twiss(beta_x)
set match(beta_y) $FODO_twiss(beta_y)
set match(alpha_x) $FODO_twiss(alpha_x)
set match(alpha_y) $FODO_twiss(alpha_y)
set match(emitt_x) 50 ; # 10^-7 m
set match(emitt_y) 50 ; # 10^-7 m
set match(phase) 0.0
set charge [expr 5.5 * 6241509074.460763 ] ; # e+, 1 nC = 6241509074.460763 e

set match(sigma_z) 1000 ; # um
set match(e_spread) 0.1 ; # percent

source $scripts/make_beam.tcl

make_beam_many beam0 $n_slice $n

FirstOrder 1

# BNS damping
set phase 0

Octave {
    AI = placet_get_number_list("$basename", "cavity");
    placet_element_set_attribute("$basename", AI, "six_dim", true);
    placet_element_set_attribute("$basename", AI, "short_range_wake", "SPARC_cav_SR");
    placet_element_set_attribute("$basename", AI, "phase", $phase);
}

SetReferenceEnergy -beamline $basename -beam beam0

Octave {
    if exist('R_${basename}_5.5nC.dat', "file")
    load 'R_${basename}_5.5nC.dat';
    else
    Response.Cx = placet_get_number_list("${basename}", "dipole");
    Response.Cy = placet_get_number_list("${basename}", "dipole");
    Response.Bpms = placet_get_number_list("${basename}", "bpm");

    # picks all correctors preceeding the last bpm, and all bpms following the first corrector
    Response.Cx = Response.Cx(Response.Cx < Response.Bpms(end));
    Response.Cy = Response.Cy(Response.Cy < Response.Bpms(end));
    Response.Bpms = Response.Bpms(Response.Bpms > max(Response.Cx(1), Response.Cy(1)));

    # gets the energy at each corrector
    Response.Ex = placet_element_get_attribute("${basename}", Response.Cx, "e0");
    Response.Ey = placet_element_get_attribute("${basename}", Response.Cy, "e0");
    
    # compute response matrices
    placet_test_no_correction("${basename}", "beam0", "Zero");
    Response.B0 = placet_get_bpm_readings("${basename}", Response.Bpms);
    Response.R0x = placet_get_response_matrix_attribute ("${basename}", "beam0", Response.Bpms, "x", Response.Cx, "strength_x", "Zero");
    Response.R0y = placet_get_response_matrix_attribute ("${basename}", "beam0", Response.Bpms, "y", Response.Cy, "strength_y", "Zero");

    save -text 'R_${basename}_5.5nC.dat' Response;
    end
}

set nm 1000
set nbins 1
set noverlap 0

set beta0 0

set sigmaq  50.0
set sigmarf 0.0
set bpmres 10.0

proc my_survey {} {
    global machine sigmaq sigmarf bpmres basename
    Octave {
	randn("seed", $machine * 1e4);
	AI = placet_get_number_list("${basename}", "cavity");
	BI = placet_get_number_list("${basename}", "bpm");
	DI = placet_get_number_list("${basename}", "dipole");
	QI = placet_get_number_list("${basename}", "quadrupole");
	placet_element_set_attribute("${basename}", DI, "strength_x", 0.0);
	placet_element_set_attribute("${basename}", DI, "strength_y", 0.0);
	placet_element_set_attribute("${basename}", BI, "resolution", $bpmres);
	placet_element_set_attribute("${basename}", AI, "x", randn(size(AI)) * $sigmarf);
	placet_element_set_attribute("${basename}", AI, "y", randn(size(AI)) * $sigmarf);
	placet_element_set_attribute("${basename}", BI, "x", randn(size(BI)) * $sigmaq);
	placet_element_set_attribute("${basename}", BI, "y", randn(size(BI)) * $sigmaq);
	placet_element_set_attribute("${basename}", QI, "x", randn(size(QI)) * $sigmaq);
	placet_element_set_attribute("${basename}", QI, "y", randn(size(QI)) * $sigmaq);
    }
}

Octave {
    EmittX_hist=zeros($nm, 2);
    EmittY_hist=zeros($nm, 2);
    Bpm0X_hist=zeros($nm, length(Response.Bpms));
    Bpm0Y_hist=zeros($nm, length(Response.Bpms));
    Bpm1X_hist=zeros($nm, length(Response.Bpms));
    Bpm1Y_hist=zeros($nm, length(Response.Bpms));
    Bpm2X_hist=zeros($nm, length(Response.Bpms));
    Bpm2Y_hist=zeros($nm, length(Response.Bpms));
    Corr2X_hist=zeros($nm, length(Response.Cx));
    Corr2Y_hist=zeros($nm, length(Response.Cy));
}

Octave {
    [E,B] = placet_test_no_correction("${basename}", "beam0", "Zero");
}

# Binning
Octave {
    nBpms = length(Response.Bpms);
    Blen = nBpms / ($nbins * (1 - $noverlap) + $noverlap);
    for i=1:$nbins
      Bmin = floor((i - 1) * Blen - (i - 1) * Blen * $noverlap) + 1;
      Bmax = floor((i)     * Blen - (i - 1) * Blen * $noverlap);
      Cxmin = find(Response.Cx < Response.Bpms(Bmin))(end);
      Cxmax = find(Response.Cx < Response.Bpms(Bmax))(end);
      Cymin = find(Response.Cy < Response.Bpms(Bmin))(end);
      Cymax = find(Response.Cy < Response.Bpms(Bmax))(end);
      Bins(i).Bpms = Bmin:Bmax;
      Bins(i).Cx = Cxmin:Cxmax;
      Bins(i).Cy = Cymin:Cymax;
    end
    printf("Each bin contains approx %g bpms.\n", round(Blen));
}

Octave {
    E0=0;
    E1=0;
    Bpm0=0;
    Bpm1=0;
}

for {set machine 1} {$machine <= $nm} {incr machine} {
    
    puts "MACHINE: $machine/$nm"
    
    my_survey
    
    Octave {
	disp("TRACKING...");
	[E,B] = placet_test_no_correction("$basename", "beam0", "None");
	if $machine == 1	
	E0 = E;
	else
	E0 += E;
	end
	EmittX_hist($machine, 1) = E(end,2);
	EmittY_hist($machine, 1) = E(end,6);
	SizeX_hist($machine, 1) = std(B(:,2));
	SizeY_hist($machine, 1) = std(B(:,3));
	Bpm0 = placet_get_bpm_readings("${basename}", Response.Bpms);
	Bpm0X_hist($machine, :) = Bpm0(:,1)';
	Bpm0Y_hist($machine, :) = Bpm0(:,2)';
	
	disp("1-TO-1 CORRECTION");
        for bin = 1:$nbins
	  Bin = Bins(bin);
	  nCx = length(Bin.Cx);
  	  nCy = length(Bin.Cy);
   	  R0x = Response.R0x(Bin.Bpms,Bin.Cx);
   	  R0y = Response.R0y(Bin.Bpms,Bin.Cy);
	  for i=1:3
	    B0 = placet_get_bpm_readings("${basename}", Response.Bpms);
	    B0 -= Response.B0;
   	    B0 = B0(Bin.Bpms,:);
            Cx = -[ R0x ; $beta0 * eye(nCx) ] \ [ B0(:,1) ; zeros(nCx,1) ];
	    Cy = -[ R0y ; $beta0 * eye(nCy) ] \ [ B0(:,2) ; zeros(nCy,1) ];
	    placet_element_vary_attribute("${basename}", Response.Cx(Bin.Cx), "strength_x", Cx);
	    placet_element_vary_attribute("${basename}", Response.Cy(Bin.Cy), "strength_y", Cy);
	    [E,B] = placet_test_no_correction("${basename}", "beam0", "None", 1, 0, Response.Bpms(Bin.Bpms(end)));
	  end
	end

	if $machine == 1	
	    E1 = E;
	else
	    E1 += E;
	end
	EmittX_hist($machine, 2) = E(end,2);
	EmittY_hist($machine, 2) = E(end,6);
	Corr1X_hist($machine, :) = placet_element_get_attribute("${basename}", Response.Cx, "strength_x");
	Corr1Y_hist($machine, :) = placet_element_get_attribute("${basename}", Response.Cy, "strength_y");
    }
}

Octave {
    E0 /= $nm;
    E1 /= $nm;
    save -text ${basename}_emitt_no_n_${nm}_5.5nC.dat E0
    save -text ${basename}_emitt_simple_b_${bpmres}_n_${nm}_5.5nC.dat E1
    save -text ${basename}_emitt_no_n_${nm}_5.5nC_hist.dat EmittX_hist EmittY_hist
    save -text ${basename}_emitt_simple_n_${nm}_5.5nC_hist.dat EmittX_hist EmittY_hist
}

Octave {
    subplot(2,1,1);
    semilogy(E0(:,1), E0(:,2)/10, 'b-', E1(:,1), E1(:,2)/10, 'r-');
    legend('NO CORRECTION', '1-TO-1 CORRECTION');
    xlabel('S [m]')
    ylabel('emitt x [mm.mrad]')
    set(gca, "linewidth", 4, "fontsize", 12);

    subplot(2,1,2);
    semilogy(E0(:,1), E0(:,6)/10, 'b-', E1(:,1), E1(:,6)/10, 'r-');
    legend('NO CORRECTION', '1-TO-1 CORRECTION');
    xlabel('S [m]')
    ylabel('emitt y [mm.mrad]')
    set(gca, "linewidth", 4, "fontsize", 12);
    print -dpng -r200 plot_emitt_5.5nC.png
}
