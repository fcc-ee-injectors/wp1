N = 10000; % number of particles

global Lstruct Ei Ef Gradient
Ei = 1540; % MeV, initial energy
Ef = 6000; % MeV, final energy
emitt = 10e-6; % m, normalized emittance
stdE_ = 0.001; % refernce energy spread
stdZ_ = 0.001; % m reference bunch length

% RF
Lstruct = 3; % m
Gradient = 25; % MV/m
frequency = 2.8; % GHz
lambda_rf = 0.299792458 / frequency; % m, wavelength
    
a_lambda = 0.15; % iris aperture radius / lambda
a = a_lambda * lambda_rf; % m, iris aperture radius

% Wakefunctions
Z0c = 0.1129409067376419; % V/pC*m, Z0 c = 0.1129409067376419 V/pC*m
W_long = @(s) Z0c ./ (pi * a * sqrt(a^2 + 8.6*s*lambda_rf)); % V/pC/m

C_LIGHT = 299792458;

global k_RF
k_RF = 2 * pi / (C_LIGHT / frequency) * 1e9; % 1 / m, 2 pi / (c / 12 GHz)

function [M,Pf] = f_Pf_std(phi_RF)

    global Ei Ef probs k_RF Z P dP_wake Lstruct Gradient
    
    % acceleration
    EG = (Ef - Ei) / cos(phi_RF); % MeV, max energy gain
    n_cav = EG / (Gradient * Lstruct); % MV, 66 is the max gradient
    
    dP_acc = EG * cos(k_RF * Z - phi_RF);
    
    Pf = P .+ dP_acc .+ n_cav * dP_wake; % final momentum        
    Pf_mean = sum(probs .* Pf);
    Pf_std = sqrt(sum(probs .* (Pf - Pf_mean).**2));
    
    M = Pf_std / Pf_mean;

end

T = [];
T_phi_RF = [];

stdZ_axis = linspace(1e-3, 4000, 150)(1:end); % um

iQe = 0;
for Qe = [ 1000 2000 4000 6000 ] % pC
    
    iQe++;
    
    dP_min = Inf;
    
    istdZ = 0;
    for stdZ = stdZ_axis / 1e6; % m
        
        istdZ++;
        global Ei Ef probs
        
        stdE = (stdE_ * stdZ_) / stdZ;
        
        stdP = Ei * stdE; % initial energy spread MeV

        % longitudinal phase space
        Nbins = 1024;
        h = linspace(-5, 5, Nbins); % sigma, -5 .. 5
        probs = normpdf(h) * range(h) / (Nbins-1); % Gaussian probability

        global Z P
        P = h * stdP + Ei; % m
        Z = h * stdZ; % m
        Q = Qe * probs; % pC

        % energy loss through wakefields
        global dP_wake
        Wl = Lstruct * W_long(Z .- min(Z)); % V/pC/m * m = V/pC
        Wl(1) *= 0.5;
        dP_wake = -conv(Q, Wl)(1:Nbins) / 1e6; % MV, energy loss for one structure

        if Q == 0
            phi_RF = 0;
        else
            phi_RF = fminbnd(@f_Pf_std, -pi/3, pi/3);
            if 0
                EG = (Ef - Ei) / cos(phi_RF); % MeV, max energy gain
                n_cav = EG / (Gradient * Lstruct) % MV, 66 is the max gradient
                [~,Pf] = f_Pf_std(phi_RF);
                scatter(Z, Pf);
                axis([ min(Z) max(Z) min(Pf) max(Pf) ]);
                drawnow
                pause(1);
            end
        end
        
        dP_std = f_Pf_std(phi_RF);
        
        if dP_std < dP_min
            dP_min = dP_std;
            phid_min = rad2deg(phi_RF);
        end
        
        T(istdZ, iQe) = dP_std;
        T_phi_RF(istdZ, iQe) = rad2deg(phi_RF);
    end

    printf("\nRESULT Qe = %g pC\n", Qe);
    dP_min
    phid_min
end

semilogy(stdZ_axis/1e3, T(:,1)*100, 'r-', 'linewidth', 2, ...
         stdZ_axis/1e3, T(:,2)*100, 'g-', 'linewidth', 2, ...
         stdZ_axis/1e3, T(:,3)*100, 'b-', 'linewidth', 2, ...
         stdZ_axis/1e3, T(:,4)*100, 'k-', 'linewidth', 2);
grid on
legend('Q = 1 nC', 'Q = 2 nC', 'Q = 3 nC', 'Q = 4 nC');
ylim([ 1e-2 1e1 ]);
xlabel('\sigma_Z [mm]');
ylabel('dP/P [%]');
print -dpng -r200 plot_espread_SR_LW_a_l_0p15.png

