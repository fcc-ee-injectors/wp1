%% This script is based on formulae from Sec 4.3 formulae of Handbook of
%% Accelerator Physics

%% LINAC 2

%% scans
Q_pC_range = linspace(2000, 6000, 60); % pC
a_lambda_range = linspace(0.09, 0.16, 70); %

iQ = 0;
for Q_pC = Q_pC_range; % pC, number of particles per bunch
    iQ++;
    
    ia = 0;
    for a_lambda = a_lambda_range
        ia++;
        
        sigmay_str = 100e-6; % m, misalignment structures
        sigmay_bpm = 30e-6; % m, misalignment bpms
        
        % Bunch train parameters
        %% Q_pC = 2e3; % pC, number of particles per bunch
        Ei = 6; % GeV, initial energy
        Ef = 20; % GeV, final energy
        emitt = 1.5e-6; % m, normalized emittance
        sigmaz = 3e-3; % m, bunch length

        % RF
        Lstr = 2; % m
        Gradient = 27.5; % MV/m
        frequency = 5.712; % GHz
        lambda_rf = 0.299792458 / frequency; % m, wavelength
        % a_lambda = 0.10; % m, iris aperture radius
        a = a_lambda * lambda_rf; % m, iris aperture radius

        % Wakefunctions
        Z0c = 0.1129409067376419; % V/pC*m, Z0 c = 0.1129409067376419 V/pC*m
        Wt = @(s) 2*Z0c*s ./ (pi * a^2 * (a^2 + s*lambda_rf)); % V/pC/m/m
        Wl = @(s) Z0c ./ (pi * a * sqrt(a^2 + 8.6*s*lambda_rf)); % V/pC/m

        % FODO
        Nstr = 4; % number of accelerating structures per FODO cell
        Lcell = Nstr*Lstr; % m, Nstr structures + 2 thin quadrupoles
        alpha = 0.5; % Lcell is proportional to \gamma^alpha
        mud = 90; % deg, phase advance per cell
        beta_average = 0.5 * Lcell * (cotd(0.5*mud) + 2/3*tand(0.5*mud)) % m

        %% Single-bunch effects
        Nparticles = Q_pC * 6241509.074460763; % e, number of particles per bunch
        G = Gradient / 0.5109989499985808; % 1/m, Gradient normalised to electron mass
        K = Nparticles / 12757618333824.26; % pC / V, pi epsilon0 * electronradius = (1 / 12757618333824.26) pC / V

        demitt_acc_rnd = @(ya) (ya * K * Wt(2*sigmaz))^2 * Lstr * beta_average / (2*alpha*G) * ((Ef/Ei)^alpha - 1); % m
        demitt_acc_sys = @(ya) (ya * K * Wt(2*sigmaz))^2 * Lcell * beta_average / (4*alpha*G) * ((Ef/Ei)^(2*alpha) - 1); % m
        demitt_bpm = @(yb) (yb * K * Wt(2*sigmaz))^2 * cosd(mud/2) / sind(mud/2)^3 * Lcell^2 / (16*alpha*G) * ((Ef/Ei)^(2*alpha) - 1); % m

        Demitt{1}(iQ, ia) = emitt + demitt_acc_rnd(sigmay_str); % m
        Demitt{2}(iQ, ia) = emitt + demitt_acc_sys(sigmay_str); % m
        Demitt{3}(iQ, ia) = emitt + demitt_bpm    (sigmay_bpm); % m
        
    end
end

for i=1:3
    Demitt{i} *= 1e6; % mm.mrad
    min_max = [ min(min(Demitt{i})) max(max(Demitt{i})) ]
end

clf
T{1} = 'random structure misalignment';
T{2} = 'systematic structure misalignment';
T{3} = 'random bpm misalignment';

%L{1} = [ 0 1 5 10 20 50 100:100:1000];
%L{2} = [ 0 10 20 50 100 200 400 1000:1000:10000 ];
%L{3} = [ 0 1 2 5 10:10:50 100:100:1000 ];

L{1} = [ 1:3:10 20:10:60 ];
L{2} = [ 1 5:5:40 48 60 100:100:350 ];
L{3} = [ 1:20 ];

for i=1:3
    figure(i, 'visible', 'off')
    clf
    [C,H] = contourf(a_lambda_range, Q_pC_range / 1e3, Demitt{i}, L{i});
    %colormap('autumn');
    title(T{i});
    xlabel('a/\lambda');
    ylabel('Bunch charge [nC]');
    clabel(C, H, 'color', 'white');
    h = colorbar;
    set(h, 'fontsize', 10);
    set(get(h,'label'), 'string', '\epsilon_{exit} [mm.mrad]','fontsize', 10);
    set(gca, 'fontsize', 10);
    print('-dpng', '-r200', sprintf('plot_scan%d.png', i));
end

system('for i in *png;  do anytopnm $i | pnmcrop -sides | pnmtopng > tmp.png && mv tmp.png $i ; done');