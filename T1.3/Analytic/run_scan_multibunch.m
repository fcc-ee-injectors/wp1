%% This script is based on formulae from Sec 4.3 formulae of Handbook of
%% Accelerator Physics

%% LINAC 2

%% scans
Q_pC_range = linspace(2000, 6000, 60); % pC
Wt_2nd_bunch_range = linspace(0, 10e3, 70); % V/pC/m/m

iQ = 0;
for Q_pC = Q_pC_range; % pC, number of particles per bunch
    iQ++;
    
    ia = 0;
    for Wt_2nd_bunch = Wt_2nd_bunch_range
        ia++;
        
        sigmay = 10e-6; % m, misalignment
        
        % Bunch train parameters
        %% Q_pC = 2e3; % pC, number of particles per bunch
        Ei = 1.54; % GeV, initial energy
        Ef = 6; % GeV, final energy
        emitt = 10e-6; % m, normalized emittance
        stdE = 0.04 * Ei; % initial energy spread MeV, 4%
        sigmaz = 3e-3; % m, bunch length
        Nbunches = 50; % number of bunches
        bunch_spacing = 17.5; % ns, bunch spacing

        % RF
        Lstr = 3; % m
        Gradient = 25; % MV/m
        frequency = 3; % GHz
        lambda_rf = 0.299792458 / frequency; % m, wavelength

        % FODO
        Nstr = 8; % number of accelerating structures per FODO cell
        Lcell = Nstr*Lstr; % m, Nstr structures + 2 thin quadrupoles
        alpha = 0.5; % Lcell is proportional to \gamma^alpha
        mud = 90; % deg, phase advance per cell
        beta_average = 0.5 * Lcell * (cotd(0.5*mud) + 2/3*tand(0.5*mud)) % m

        %% Single-bunch effects
        Nparticles = Q_pC * 6241509.074460763; % e, number of particles per bunch
        G = Gradient / 0.5109989499985808; % 1/m, Gradient normalised to electron mass
        K = Nparticles / 12757618333824.26; % pC / V, pi epsilon0 * electronradius = (1 / 12757618333824.26) pC / V

        % Wakefunctions
        % a_lambda = 0.10; %
        % a = a_lambda * lambda_rf; % m, iris aperture radius
        Z0c = 0.1129409067376419; % V/pC*m, Z0 c = 0.1129409067376419 V/pC*m
        Wt = @(s) 2*Z0c*s ./ (pi * a^2 * (a^2 + s*lambda_rf)); % V/pC/m/m
        Wl = @(s) Z0c ./ (pi * a * sqrt(a^2 + 8.6*s*lambda_rf)); % V/pC/m

        if 0
            demitt_acc_rnd = @(ya) (ya * K * Wt(2*sigmaz))^2 * Lstr * beta_average / (2*alpha*G) * ((Ef/Ei)^alpha - 1); % m
            demitt_acc_sys = @(ya) (ya * K * Wt(2*sigmaz))^2 * Lcell * beta_average / (4*alpha*G) * ((Ef/Ei)^(2*alpha) - 1); % m
            demitt_bpm = @(yb) (yb * K * Wt(2*sigmaz))^2 * cosd(mud/2) / sind(mud/2)^3 * Lcell^2 / (16*alpha*G) * ((Ef/Ei)^(2*alpha) - 1); % m
            
            Demitt{1}(iQ, ia) = demitt_acc_rnd(sigmay) / emitt * 100;
            Demitt{2}(iQ, ia) = demitt_acc_sys(sigmay) / emitt * 100;
            Demitt{3}(iQ, ia) = demitt_bpm    (sigmay) / emitt * 100;
        end
        
        %% Multi-bunch effects
        % Wt_2nd_bunch [V/pC/m/m]

        A = @(Wt_2nd_bunch) abs(2*K * Wt_2nd_bunch * beta_average / (alpha * G)) * (Ef/Ei)^alpha; % #

        Demitt{1}(iQ, ia) = A(Wt_2nd_bunch);

        %% Misaligned accelerator structures and a corrected trajectory

        clight = 0.299792458; % m/ns, speed of light
        Wt_train = zeros(1, Nbunches); % V/pC/m/m
        Wt_train(2) = Wt_2nd_bunch; % V/pC/m/m
        Wsum = sqrt(sum(Wt_train.^2) / Nbunches - (sum(Wt_train) / Nbunches)^2); % V/pC/m/m 

        demitt_MB_acc = @(ya) (ya * K * 4 * Wsum)^2 * Lstr * beta_average / (2*alpha*G) * ((Ef/Ei)^alpha - 1); % m

        Demitt{2}(iQ, ia) = demitt_MB_acc(sigmay) / emitt * 100;

    end
end

clf
T{1} = 'action amplification';
T{2} = 'emittance growth';

for i=1:2
    figure(i)
    clf
    [C,H] = contourf(Wt_2nd_bunch_range / 1e3, Q_pC_range / 1e3, Demitt{i});
    title(T{i});
    xlabel('W_t at 2^{nd} bunch [V/pC/m/mm]');
    ylabel('Bunch charge [nC]');
    clabel(C, H, 'color', 'black');
    h = colorbar;
    set(h, 'fontsize', 10);
    if i==1
        bar_title = '\Delta{A}/A_0';
    else
        bar_title = '\Delta\epsilon/\epsilon [%]';
    end
    set(get(h,'label'), 'string', bar_title, 'fontsize', 10);
    set(gca, 'fontsize', 10);
    print('-dpng', '-r200', sprintf('plot_scan%d.png', 3+i));
end

