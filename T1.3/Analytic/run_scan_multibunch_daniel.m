%% Linac parameters
%L_linac = 13 * 8 * 0.5; % m, 13 RF units x 8 RF structures / unit x 0.5 m length each RF structure
L_Ei = 1.54; % GeV
L_Ef = 6; % GeV
L_grad = 25e6; % V/m
L_beta = 20; % m

N_bunches = 50;

%% Integral
% E(s) := Ei + (Ef - Ei) * (s - Si) / (Sf - Si);
% Integral = 0.5 * integrate(beta / (E(s)), s, Si, Sf);
Integral = (log(L_Ef/L_Ei)*L_beta)/L_grad/2; % m^2/V

%% Bunch parameters
X1 = linspace(0, 10, 37); % V/pC/m/mm
X2 = linspace(0, 4000, 31); % pC
T_coh = zeros(length(X1), length(X2));
T_rms = zeros(length(X1), length(X2));
T_worst = zeros(length(X1), length(X2));

Ti = 1;
for Wt = X1 % V/pC/m/mm
    
    % Wakefield 
    V_sb = zeros(1, N_bunches); % V/pC/m/mm
    V_sb(2) = Wt; % V/pC/mm/m

    Tj = 1;
    for bunch_charge = X2 % pC
        
        A_sb = sqrt(-1) * V_sb * bunch_charge * Integral * 1000; % #
        A = zeros(N_bunches);
        for i=1:N_bunches
            A(i:end,i) = A_sb(1:end-i+1);
        end
        expA = expm(A);
        
        F_coh = mean(abs(expA  * ones(N_bunches,1)).**2);
        F_rms = mean(sum(expA .* conj(expA)));
        F_worst = svd(expA)(1)**2;
        
        T_coh  (Ti,Tj) = F_coh - 1;
        T_rms  (Ti,Tj) = F_rms - 1;
        T_worst(Ti,Tj) = F_worst - 1;
        
        Tj++;
    end
    Ti++
end

figure(1)
[C,H]=contourf(X1, X2/1e3, T_coh');
clabel(C,H, "color", "black");
colorbar
title('F_{coh}');
xlabel('Wt at second bunch [V/pC/m/mm]');
ylabel('Bunch charge [nC]');
%zlabel('F_{coh}');
set(gca, "fontsize", 12);
print -dpng -r200 plot_scan_charge_Q_coh.png

[C,H]=contourf(X1, X2/1e3, T_rms');
clabel(C,H, "color", "black");
colorbar
title('F_{rms}');
xlabel('Wt at second bunch [V/pC/m/mm]');
ylabel('Bunch charge [nC]');
%zlabel('F_{coh}');
set(gca, "fontsize", 12);
print -dpng -r200 plot_scan_charge_Q_rms.png

[C,H]=contourf(X1, X2/1e3, T_worst');
clabel(C,H, "color", "black");
colorbar
title('F_{worst}');
xlabel('Wt at second bunch [V/pC/m/mm]');
ylabel('Bunch charge [nC]');
%zlabel('F_{coh}');
set(gca, "fontsize", 12);
print -dpng -r200 plot_scan_charge_Q_worst.png
