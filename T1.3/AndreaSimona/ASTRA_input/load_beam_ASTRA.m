function [B0,P0] = load_beam_ASTRA(filename, Q_nC, stride)
    RF_Track;
    T = load(filename);
    T(2:end,[ 3 6 7 ]) += T(1,[ 3 6 7 ]); % ASTRA convention
    if nargin==1
        Q_nC = sum(T(:,8)) % 
    end
    if nargin>=3
        T = T(1:stride:end,:);
    end
    % x xp y yp dt P
    mass = RF_Track.electronmass; % MeV/c^2
    X = T(:,1) * 1e3; % mm;
    Y = T(:,2) * 1e3; % mm;
    Z = T(:,3) * 1e3; % mm;
    Z -= Z(1);
    Px = T(:,4) / 1e6; % MeV/c
    Py = T(:,5) / 1e6; % MeV/c
    Pz = T(:,6) / 1e6; % MeV/c
    P = hypot(Px, Py, Pz); % MeV/c
    E = hypot(mass, P); % MeV
    Vx = Px ./ E; % c
    Vy = Py ./ E; % c
    Vz = Pz ./ E; % c
    % move particles to Z = 0;
    dt = -Z ./ Vz;
    X += Vx .* dt;
    Y += Vy .* dt;
    % create a 9-column matrix for Bunch6dT
    B = zeros(size(T,1), 9);
    B(:,1) = X;              % mm
    B(:,2) = Px ./ Pz * 1e3; % mrad
    B(:,3) = Y;              % mm
    B(:,4) = Py ./ Pz * 1e3; % mrad
    B(:,5) = dt;             % mm/c
    B(:,6) = P;              % MeV/c
    B(:,7) = mass; % MeV/c^2
    B(:,8) = -1; % e^+
    B(:,9) = abs(Q_nC) * RF_Track.nC / size(T,1); % e+
    B0 = Bunch6d(B); % beam
    P0 = Bunch6d(B(1,:)); % reference particle
