close all
clear all

RF_Track;

%%%%%%%%%%%%%%% To control the simulations

%ratio a/lambda, with a radius of the cavity
a_lambda = 0.1; %[0.1:0.05:0.2]; %[0.1:0.025:0.2];
f_rf = 2.8e9;
gradient = 25e6; %MV/m
lambda = RF_Track.clight / f_rf; % m
L_cell = lambda / 3; % m
N_cells = 84; % must be multiple of three
L_structure = N_cells*L_cell; %m anche piu' corte
rf_period = RF_Track.clight*1e3/f_rf; % mm/c
voltage = gradient*L_structure; %MV
a_rf = a_lambda*lambda; % m
start_r = a_rf*1e3; % in mm 12.695;
end_r = a_rf*1e3; % in mm 9.31;

%Charge
N_bunches = 2;
Q_C = 4e-9 %C
population = Q_C * RF_Track.C; % e
charge = -1; % e
mass = RF_Track.electronmass; % MeV/c^2

%FODO
L_drift_FODO = 0.25; %m
L_quad = 0.25; %m %25 mm apertura, L = 8e-2 m, G = 40 T/m
% K1_FODO = 0.4; %1/m2 Andrea
%Phase advance
Dmu = pi/2;
L_FODO = 4*L_drift_FODO+2*L_quad+2*L_structure;
K1_FODO = 1.477351109;

% Lattice params
phase = 90

Pinitial = 1500; % MeV/c
Pfinal = 6000; % MeV/c

%% Define the short-range wakefield
g = L_cell - 2.5e-3; % m
SRWF = ShortRangeWakefield (a_rf, g, L_cell);

%I define the beam parameters
n_periods = 49; % 17.5 ns at 2.8 GHz
S_bunch = rf_period * n_periods / 1e3; % m, intra-bunch separation


T_A = [];
%% Define the long-range wakefield
for Kick_t = linspace(-1, 1, 21) % V/pC/m/mm
  Kick_l = 0; % V/pC/m
  
  Wt = [ 0 Kick_t 0 ]; % V/pC/m/mm
  Wl = [ 0.5*Kick_l Kick_l 0 ]; % V/pC/m
  
  LRWF = Wakefield_1d(Wt, Wl, S_bunch);
  
  %% Define lattice
  FCC_L2 = Lattice();
  
  P = Pinitial; % MeV/c
  while P < Pfinal
    
    TW = TW_Structure (gradient, 0, f_rf, 2*pi/3, -N_cells);
    TW.set_phid (phase-90);
    TW.set_nsteps (10);
    %TW.add_collective_effect(SRWF);
    TW.add_collective_effect(LRWF);
    TW.set_cfx_nsteps(10);
    
    Q = Quadrupole (L_quad, P / -1, -K1_FODO);
    D = Drift (L_drift_FODO);
    
    FCC_L2.append (Q);
    FCC_L2.append (D);
    FCC_L2.append (TW);
    FCC_L2.append (D);
    
    P += voltage / 1e6 * sind(phase);
    
    K1_FODO = -K1_FODO;
    
  end
  
  disp('The lattice has been created.')
  
  disp(['Linac length = ' num2str(FCC_L2.get_length()) ' m']);
  disp(['P final = ', num2str(P) ]);
  
  % Set RF phases
  P0 = Bunch6d(mass, population, charge, [ 0 0 0 0 0 Pinitial ]);
  FCC_L2.autophase(P0);
  
  %% Create the beam
  Angled_range = linspace(0, 360, 36); % deg
  sigma_x = 0.051075; % mm
  sigma_xp = 0.022906; % mrad
  
  clear T
  iA = 1;
  for Angled = Angled_range
    
    printf('Angle %d/%d\n', iA, length(Angled_range));
    
    Dx  = 0.1 * sigma_x  * cosd(Angled); % mm
    Dxp = 0.1 * sigma_xp * sind(Angled); % mrad
    
    M0_ = [ Dx Dxp 0 0 0 Pinitial ];
    M0 = [];
    for n=1:N_bunches
      M0 = [ M0 ; M0_ ];
      M0_(:,5) += S_bunch*1e3; % mm/c
    end
    
    B0 = Bunch6d(mass, population * N_bunches, charge, M0);
    B1 = FCC_L2.track(B0);
    
    %% Retrieve the phase space coordinate
    M0 = B0.get_phase_space('%x %Px');
    M1 = B1.get_phase_space('%x %Px');
    
    % prepares data structure
    for iB=1:N_bunches
      A0_{iB}(iA,1) = M0(iB,1);
      A0_{iB}(iA,2) = M0(iB,2);
      A1_{iB}(iA,1) = M1(iB,1);
      A1_{iB}(iA,2) = M1(iB,2);
    end        
    
    iA++;
  end
  
  clear A0 A1
  for iB = 1:N_bunches
    A0(iB) = polyarea(A0_{iB}(:,1), A0_{iB}(:,2));
    A1(iB) = polyarea(A1_{iB}(:,1), A1_{iB}(:,2));
  end
  
  T_A = [ T_A ; Kick_t max(A1 ./ A0) ];
end

%% Make plots
figure(1)
clf
hold on
plot(T_A(:,1), T_A(:,2), 'linewidth', 2);
plot(T_A(:,1), 1.1 * ones(size(T_A(:,1))), 'k--');
xlabel('W_t [V/pC/m/mm]');
ylabel('A_{final} / A_{initial}');
ylim([ 0.9 1.5 ]);
print -dpng plot_multi_bunch_action_scan.png
