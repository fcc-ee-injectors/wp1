close all
clear all

RF_Track;

a_lambda = 0.2; %[0.1:0.05:0.2]; %[0.1:0.025:0.2];
phase = 80; %[70:1:80]%[90-6:1:90]; %[80:1:90] %deg
f_rf = 2.8e9;
gradient = 25e6; %MV/m
lambda = RF_Track.clight / f_rf; % m
L_cell = lambda / 3; % m
N_cells = 84; % must be multiple of three
L_structure = N_cells*L_cell; %m anche piu' corte
voltage = gradient*L_structure; %MV
file_wake_l = 'wake_l.sdds';
file_wake_t = 'wake_t.sdds';
%frequency
%S-band
%radius of the cavity
a_rf = a_lambda*lambda; % m
start_r = a_rf*1e3; % in mm 12.695;
end_r = a_rf*1e3; % in mm 9.31;


%Charge
Q_C = 4e-9 %C

%FODO
L_drift_FODO = 0.25; %m
L_quad = 0.25; %m %25 mm apertura, L = 8e-2 m, G = 40 T/m
               % K1_FODO = 0.4; %1/m2 Andrea
               %Phase advance
Dmu = pi/2;
L_FODO = 4*L_drift_FODO+2*L_quad+2*L_structure;
K1_FODO = (4*sin(Dmu/2))/(L_FODO*L_quad);
K1_FODO = 0.746173319;

g = L_cell - 2.5e-3; % m

SRWF = ShortRangeWakefield (a_rf, g, L_cell);

%I start to create the model
phase = 90
    
Pinitial = 1500; % MeV/c
Pfinal = 6000; % MeV/c

FCC_L2 = Lattice();

P = Pinitial; % MeV/c

%% Write lattice
while P < Pfinal
    
    TW = TW_Structure (gradient, 0, f_rf, 2*pi/3, -N_cells);
    TW.set_phid (phase-90);
    TW.set_nsteps (10);
    TW.add_collective_effect(SRWF);
    TW.set_cfx_nsteps(10);
    
    Q = Quadrupole (L_quad, P / -1, -K1_FODO);
    D = Drift (L_drift_FODO);
    
    FCC_L2.append (Q);
    FCC_L2.append (D);
    FCC_L2.append (TW);
    FCC_L2.append (TW);
    FCC_L2.append (D);
    
    P += voltage / 1e6 * sind(phase);
    P += voltage / 1e6 * sind(phase);
    
    K1_FODO = -K1_FODO;
    
end

disp('The lattice file has been created.')

disp(['Linac length = ' num2str(FCC_L2.get_length()) ' m']);
disp(['P final = ', num2str(P) ]);

%% Create bunch
mass = RF_Track.electronmass; % MeV/c^2
population = Q_C * RF_Track.C; % e
charge = -1; % e

Twiss = Bunch6d_twiss();
Twiss.emitt_x = 3; % mm.mrad, normalized emittance
Twiss.emitt_y = 3; % mm.mrad
Twiss.beta_x = 4.6953558; % m
Twiss.beta_y = 25.65234507; % m
Twiss.alpha_x = 0.4888058501;
Twiss.alpha_y = -2.401813359;
Twiss.sigma_t = 1.3; % mm/c
Twiss.sigma_d = 0.1; % permill

%% Create the bunch
B0 = Bunch6d(mass, population, charge, Pinitial, Twiss, 10000);
P0 = Bunch6d(mass, population, charge, [ 0 0 0 0 0 Pinitial ]);

tic
P_autophase = FCC_L2.autophase(P0)
B1 = FCC_L2.track(B0);
toc

T = FCC_L2.get_transport_table('%S %mean_E %beta_x %beta_y %emitt_x %emitt_y');
M = B1.get_phase_space('%x %xp %y %yp %dt %P');

%
figure(1);
clf
hold on
plot(T(:,1), T(:,3), 'b-', 'linewidth', 2);
plot(T(:,1), T(:,4), 'r-', 'linewidth', 2);
lgnd = legend({ '\beta_x ', '\beta_y ' });
xlabel('S [m]');
ylabel('\beta [m]');
legend boxoff;
legend boxon;
set(lgnd, 'fontsize', 14);
set(gca, 'linewidth', 2, 'fontsize', 14);
print -dpng plot_beta.png

figure(2);
clf
hold on
plot(T(:,1), T(:,5), 'b-', 'linewidth', 2);
plot(T(:,1), T(:,6), 'r-', 'linewidth', 2);
lgnd = legend({ '\epsilon_x ', '\epsilon_y ' });
xlabel('S [m]');
ylabel('\epsilon [mm.mrad]');
legend boxoff;
legend boxon;
set(lgnd, 'fontsize', 14);
set(gca, 'linewidth', 2, 'fontsize', 14);
box;
print -dpng plot_emitt.png

figure(3);
clf
plot(T(:,1), T(:,2), 'b-', 'linewidth', 2);
xlabel('S [m]');
ylabel('E [MeV]');
set(gca, 'linewidth', 2, 'fontsize', 14);
print -dpng plot_E.png

%% Make plots
figure(4);
clf
scatter(M(:,5), M(:,6), '*');
xlabel('\Delta{t} [mm/c]');
ylabel('P [MeV/c]');
axis( [ min(M(:,5)) max(M(:,5)) min(M(:,6)) max(M(:,6)) ]);
set(gca, 'linewidth', 2, 'fontsize', 14);
print -dpng plot_tP.png

figure(5)
clf
hold on
scatter(M(:,1), M(:,2), '*');
xlabel('x [mm]');
ylabel('x'' [mrad]');
set(gca, 'linewidth', 2, 'fontsize', 14);
print -dpng plot_xxp.png
