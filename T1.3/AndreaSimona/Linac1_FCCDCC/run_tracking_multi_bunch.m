close all
clear all

RF_Track;

f_rf = 2e9;
gradient = 25e6; %MV/m
lambda = RF_Track.clight / f_rf; % m
L_cell = lambda / 3; % m
N_cells = 60; % must be multiple of three
L_structure = N_cells*L_cell; %m anche piu' corte
rf_period = RF_Track.clight*1e3/f_rf; % mm/c
voltage = gradient*L_structure; %MV
file_wake_l = 'wake_l.sdds';
file_wake_t = 'wake_t.sdds';
%frequency
%S-band
%radius of the cavity
a_lambda = 0.2;
a_rf = a_lambda*lambda; % m
start_r = a_rf*1e3; % in mm 12.695;
end_r = a_rf*1e3; % in mm 9.31;


%Charge
Q_C = 4e-9 %C

%FODO
L_drift_FODO = 0.25; %m
L_quad = 0.25; %m %25 mm apertura, L = 8e-2 m, G = 40 T/m
               % K1_FODO = 0.4; %1/m2 Andrea
               %Phase advance
Dmu = pi/2;
L_FODO = 4*L_drift_FODO+2*L_quad+2*L_structure;
K1_FODO = 0.746173319;


%For the multi-bunch
%I take the time as close as possible to 17.5 ns but staying at the same phase as the first
%bunch
% f_rf = f_rf*1.335 just for test
dt_bunch_target = 17.5e-9;
N_full_periods = fix(dt_bunch_target/(1/f_rf));
Rest = mod(dt_bunch_target,(1/f_rf));
% Rest = dt_bunch-N_full_periods*1/f_rf;
dt_bunch = N_full_periods*1/f_rf;




%I build the lattice

%I define the short-range wakefield
g = L_cell - 2.5e-3; % m
SRWF = ShortRangeWakefield (a_rf, g, L_cell);

%I define the long-range wakefield
n_periods = 49;
S_bunch = rf_period * n_periods / 1e3; % m, intra-bunch separation

Kick_t = -1; % V/pC/m/mm
Kick_l = 100; % V/pC/m

Wt = [ 0 Kick_t 0 ]; % V/pC/m/mm
Wl = [ 0.5*Kick_l Kick_l 0 ]; % V/pC/m

LRWF = Wakefield_1d(Wt, Wl, S_bunch);


%I start to create the model
phase = 90
    
Pinitial = 200; % MeV/c
Pfinal = 1500; % MeV/c

FCC_L2 = Lattice();

P = Pinitial; % MeV/c

%% Write lattice
while P < Pfinal
    
    TW = TW_Structure (gradient, 0, f_rf, 2*pi/3, -N_cells);
    TW.set_phid (phase-90);
    TW.set_nsteps (10);
    %TW.add_collective_effect(SRWF);
    TW.add_collective_effect(LRWF);
    TW.set_cfx_nsteps(10);
    
    Q = Quadrupole (L_quad, P / -1, -K1_FODO);
    D = Drift (L_drift_FODO);
    
    FCC_L2.append (Q);
    FCC_L2.append (D);
    FCC_L2.append (TW);
    FCC_L2.append (TW);
    FCC_L2.append (D);
    
    P += voltage / 1e6 * sind(phase);
    P += voltage / 1e6 * sind(phase);
    
    K1_FODO = -K1_FODO;
    
end

disp('The lattice file has been created.')

disp(['Linac length = ' num2str(FCC_L2.get_length()) ' m']);
disp(['P final = ', num2str(P) ]);

%% Create train
N_bunches = 20;
mass = RF_Track.electronmass; % MeV/c^2
population = Q_C * RF_Track.C; % e
charge = -1; % e


DX = 10; % mm

M0_ = [ DX 0 0 0 0 Pinitial ];
M0 = [];
for n=1:N_bunches
    M0 = [ M0 ; M0_ ];
    M0_(:,5) += S_bunch*1e3; % mm/c
end

B0 = Bunch6d(RF_Track.electronmass, population * N_bunches, charge, M0);
P0 = Bunch6d(mass, population, charge, [ 0 0 0 0 0 Pinitial ]);

tic
P_autophase = FCC_L2.autophase(P0)
B1 = FCC_L2.track(B0);
toc

M1 = B1.get_phase_space();

%% Retrieve the Twiss plot and the phase space
T = FCC_L2.get_transport_table('%S %mean_x %mean_y %mean_E');

figure(1)
clf
hold on
plot(M1(:,1), 'b-*', 'linewidth', 2);
plot(M1(:,2), 'r-*', 'linewidth', 2);
lgnd = legend({ '<x> [mm]', '<x''> [mrad] ' });
xlabel('bunch #');
ylabel('offset at linac end');
legend boxoff
legend boxon
set(lgnd, 'fontsize', 14);
set(gca, 'linewidth', 2, 'fontsize', 14);
box
print -dpng plot_train.png

figure(2)
clf
plot(M1(:,6)/1e3, 'r-*', 'linewidth', 2);
axis([ 0 N_bunches 3 7 ] )
lgnd = legend({ '<P>' });
xlabel('bunch #');
ylabel('P [GeV/c]');
legend boxoff
legend boxon
set(lgnd, 'fontsize', 14);
print -dpng plot_train_P.png

figure(3)
clf
hold on
plot(T(:,1), T(:,4)/1e3, 'r-*', 'linewidth', 2);
xlabel('S [m]');
ylabel('E [GeV]');
set(gca, 'linewidth', 2, 'fontsize', 14);
box
print -dpng plot_energy_train_average.png
