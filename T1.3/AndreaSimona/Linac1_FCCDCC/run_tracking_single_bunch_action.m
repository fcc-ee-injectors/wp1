close all
clear all

RF_Track;

for a_lambda = [ 0.10 0.15 0.2 ];
    f_rf = 2e9;
    gradient = 25e6; %V/m
    lambda = RF_Track.clight / f_rf; % m
    L_cell = lambda / 3; % m
    N_cells = 60; % must be multiple of three
    L_structure = N_cells*L_cell; %m anche piu' corte
    voltage = gradient*L_structure; %V
    a_rf = a_lambda*lambda; % m

    %Charge
    Q_C = 4e-9 %C

    %FODO
    L_drift_FODO = 0.25; %m
    L_quad = 0.25; %m %25 mm apertura, L = 8e-2 m, G = 40 T/m
                   % K1_FODO = 0.4; %1/m2 Andrea
                   %Phase advance
    Dmu = pi/2;
    L_FODO = 4*L_drift_FODO+2*L_quad+2*L_structure;
    K1_FODO = (4*sin(Dmu/2))/(L_FODO*L_quad);
    K1_FODO = 0.746173319;


    %I build the lattice

    %I define the wakefield
    % Structure parameters: a gradient of the aperture is assumed

    g = L_cell - 2.5e-3; % m

    SRWF = ShortRangeWakefield (a_rf, g, L_cell);

    %I start to create the model
    phase = 90
    
    Pinitial = 200; % MeV/c
    Pfinal = 1500; % MeV/c

    FCC_L2 = Lattice();

    P = Pinitial; % MeV/c

    %% Write lattice
    while P < Pfinal
        
        TW = TW_Structure (gradient, 0, f_rf, 2*pi/3, -N_cells);
        TW.set_phid (phase-90);
        TW.set_nsteps (10);
        TW.add_collective_effect(SRWF);
        TW.set_cfx_nsteps(10);
        
        Q = Quadrupole (L_quad, P / -1, -K1_FODO);
        D = Drift (L_drift_FODO);
        
        FCC_L2.append (Q);
        FCC_L2.append (D);
        FCC_L2.append (TW);
        FCC_L2.append (TW);
        FCC_L2.append (D);
        
        P += voltage / 1e6 * sind(phase);
        P += voltage / 1e6 * sind(phase);
        
        K1_FODO = -K1_FODO;
        
    end

    disp('The lattice has been created.')

    disp(['Linac length = ' num2str(FCC_L2.get_length()) ' m']);
    disp(['P final = ', num2str(P) ]);

    %% Create bunch
    mass = RF_Track.electronmass; % MeV/c^2
    population = Q_C * RF_Track.C; % e
    charge = -1; % e

    Twiss = Bunch6d_twiss();
    Twiss.emitt_x = 3; % mm.mrad, normalized emittance
    Twiss.emitt_y = 3; % mm.mrad
    Twiss.beta_x = 4.6953558; % m
    Twiss.beta_y = 25.65234507; % m
    Twiss.alpha_x = 0.4888058501;
    Twiss.alpha_y = -2.401813359;
    Twiss.sigma_t = 1.3; % mm/c
    Twiss.sigma_d = 0.1; % permill

    %% Create the bunch
    B0 = Bunch6d(mass, population, charge, Pinitial, Twiss, 10000);
    P0 = Bunch6d(mass, population, charge, [ 0 0 0 0 0 Pinitial ]);

    M0 = B0.get_phase_space();
    P_autophase = FCC_L2.autophase(P0)

    Angled_range = linspace(0, 360, 36); % deg
    sigma_x = std(M0(:,1)); % mm
    sigma_xp = std(M0(:,2)); % mrad

    for i = 1:length(Angled_range)
        
        Angled = Angled_range(i);
        
        printf('Angle %d/%d\n', i, length(Angled_range));
        
        M = M0;
        M(:,1) += 0.1 * sigma_x * cosd(Angled);
        M(:,2) += 0.1 * sigma_xp * sind(Angled);
        B0.set_phase_space(M);
        B1 = FCC_L2.track(B0);
        
        T(i,:,:) = FCC_L2.get_transport_table(['%S %mean_x %mean_y %mean_xp %mean_yp %mean_P']);
    end

    Ax = [];
    Ay = [];
    for i = 1:size(T,2)
        Ax = [ Ax ; polyarea(T(:,i,2), T(:,i,4).*T(:,i,6)) ];
        Ay = [ Ay ; polyarea(T(:,i,3), T(:,i,5).*T(:,i,6)) ];
    end
    Ax /= Ax(1);
    Ay /= Ax(1);

    A = [ T(1,:,1)' Ax Ay ];

    figure(1)
    hold on
    plot(A(:,1), A(:,2), 'linewidth', 2);
end

plot(A(:,1), 1.1 * ones(size(A(:,1))), 'k--');
xlabel('S [m]');
ylabel('A_{final} / A_{initial}');
legend('a/\lambda = 0.1', 'a/\lambda = 0.15', 'a/\lambda = 0.2', ...
       'location', 'northwest');
print -dpng plot_single_bunch_action.png
